<?php

namespace App\Http\Controllers\Setting;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use GuzzleHttp\Client;
use Validator;
use App\Models\Akses;
use App\Models\Role;
use App\Models\ViewAkses;
use App\Models\ViewModul;
class AksesController extends Controller
{
    
    public function index(request $request)
    {
        if(Auth::user()->role_id==1){
            if($request->role_id>0){
                $role=$request->role_id;
            }else{
                $role=1;
            }
            return view('setting.akses.index',compact('role'));
        }else{
            return view('error');
        }
         
       
        
    }
    
    
    public function view(request $request)
    {
        error_reporting(0);
        $template='top';
        $id=decoder($request->id);
        $data=Peminjaman::find($id);
        
        if($id>0){
            $disabled='readonly';
            
        }else{
            $disabled='readonly';
        }
        if(Auth::user()->role_id==1){
            return view('setting.role.view',compact('template','data','disabled','id'));
        }else{
            return view('error');
        }
        
        
        
    }
    public function modal(request $request)
    {
        error_reporting(0);
        $template='top';
        $id=decoder($request->id);
        $data=Role::find($id);
        
        if($id>0){
            $disabled='readonly';
            
            
        }else{
            $disabled='readonly';
            
        }
        if(Auth::user()->role_id==1){
            return view('setting.role.modal',compact('template','data','disabled','id'));
        }else{
            return view('error');
        }
        
        
        
    }
    
    
    public function get_data(request $request)
    {
        error_reporting(0);
        $role_id=$request->role_id;
        $data = ViewModul::whereIn('active',array(1,0))->orderBy('id','Asc')->get();
        // $data = ViewRole::whereIn('active',array(1,0))->orderBy('nama_aset','Asc')->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn='
                <div class="btn-group btn-group-sm ">
                    <a href="#" data-toggle="dropdown" class="btn btn-success btn-xs dropdown-toggle" title="Pilih proses"><i class="fas fa-cog fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="javascript:;" class="dropdown-item" onclick="tambah(`'.encoder($row->id).'`)"><i class="fas fa-pencil-alt fa-fw"></i> Ubah</a>
                        <div class="dropdown-divider"></div>
                        <a href="javascript:;" class="dropdown-item" onclick="delete_data(`'.encoder($row->id).'`)"><i class="fas fa-trash-alt fa-fw"></i> Hapus</a>
                    </div>
                </div>
                ';
                return $btn;
            })
            
            ->addColumn('status', function ($row) use ($role_id) {
                if(cek_akses_modul($role_id,$row->id)>0){
                    $btn='<div class="custom-control custom-switch mb-1">
                        <input type="checkbox" class="custom-control-input" onclick="switch_data('.$role_id.','.$row->id.',0)" id="customSwitch'.$row->id.'" checked>
                        <label class="custom-control-label" for="customSwitch'.$row->id.'"></label>
                    </div>';
                }else{
                    $btn='<div class="custom-control custom-switch mb-1">
                        <input type="checkbox" class="custom-control-input" onclick="switch_data('.$role_id.','.$row->id.',1)" id="customSwitch'.$row->id.'" >
                        <label class="custom-control-label" for="customSwitch'.$row->id.'"></label>
                    </div>';
                }
                
                
                return $btn;
            })
           
            
            ->rawColumns(['action','act','status'])
            ->make(true);
    }
    public function get_data_non(request $request)
    {
        error_reporting(0);
        $data = ViewRole::whereIn('active',array(1,0))->where('kategori_aset_id','!=',2)->orderBy('nama_aset','Asc')->get();
        // $data = ViewRole::whereIn('active',array(1,0))->orderBy('nama_aset','Asc')->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn='
                <div class="btn-group btn-group-sm ">
                    <a href="#" data-toggle="dropdown" class="btn btn-success btn-xs dropdown-toggle" title="Pilih proses"><i class="fas fa-cog fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="javascript:;" class="dropdown-item" onclick="tambah(`'.encoder($row->id).'`)"><i class="fas fa-pencil-alt fa-fw"></i> Ubah</a>
                        <div class="dropdown-divider"></div>
                        <a href="javascript:;" class="dropdown-item" onclick="delete_data(`'.encoder($row->id).'`)"><i class="fas fa-trash-alt fa-fw"></i> Hapus</a>
                    </div>
                </div>
                ';
                return $btn;
            })
            ->addColumn('act', function ($row) {
                $btn='<input type="checkbox" name="nik[]" value="'.$row->nik.'">';
                
                return $btn;
            })
            ->addColumn('harga', function ($row) {
                
                return uang($row->harga);
            })
            ->addColumn('status', function ($row) {
                if($row->active==1){
                    $btn='<div class="custom-control custom-switch mb-1">
                        <input type="checkbox" class="custom-control-input" onclick="switch_data('.$row->id.',0)" id="customSwitch'.$row->id.'" checked>
                        <label class="custom-control-label" for="customSwitch'.$row->id.'"></label>
                    </div>';
                }else{
                    $btn='<div class="custom-control custom-switch mb-1">
                        <input type="checkbox" class="custom-control-input" onclick="switch_data('.$row->id.',1)" id="customSwitch'.$row->id.'" >
                        <label class="custom-control-label" for="customSwitch'.$row->id.'"></label>
                    </div>';
                }
                
                
                return $btn;
            })
           
            
            ->rawColumns(['action','act','status'])
            ->make(true);
    }
    public function get_data_aset(request $request)
    {
        error_reporting(0);
        $data = ViewRole::whereIn('active',array(1,0))->orderBy('nama_aset','Asc')->get();
        // $data = ViewRole::whereIn('active',array(1,0))->orderBy('nama_aset','Asc')->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn='<span class="btn btn-xs btn-primary" onclick="pilih_aset('.$row->id.',`0`)">Pilih</span>';
                return $btn;
            })
            ->addColumn('act', function ($row) {
                $btn='<input type="checkbox" name="nik[]" value="'.$row->nik.'">';
                
                return $btn;
            })
            ->addColumn('qris', function ($row) {
                $btn=barcoderr(encoder($row->id));
                
                return $btn;
            })
            ->addColumn('harga', function ($row) {
                
                return uang($row->harga);
            })
            ->addColumn('status', function ($row) {
                if($row->active==1){
                    $btn='<div class="custom-control custom-switch mb-1">
                        <input type="checkbox" class="custom-control-input" onclick="switch_data('.$row->id.',0)" id="customSwitch'.$row->id.'" checked>
                        <label class="custom-control-label" for="customSwitch'.$row->id.'"></label>
                    </div>';
                }else{
                    $btn='<div class="custom-control custom-switch mb-1">
                        <input type="checkbox" class="custom-control-input" onclick="switch_data('.$row->id.',1)" id="customSwitch'.$row->id.'" >
                        <label class="custom-control-label" for="customSwitch'.$row->id.'"></label>
                    </div>';
                }
                
                
                return $btn;
            })
           
            
            ->rawColumns(['action','act','status','qris'])
            ->make(true);
    }
    
    
    public function delete_data(request $request){
        if(Auth::user()->role_id==1 || Auth::user()->role_id==4){
            $id=decoder($request->id);
            $data = Role::where('id',$id)->update(['active'=>2]);
        }
        

    }
    public function switch_status(request $request){
        if($request->act==1){
            $data = Akses::UpdateOrCreate([
                'modul_id'=>$request->id,
                'role_id'=>$request->role_id,
            ]);
        }else{
            $data = Akses::where('modul_id',$request->id)->where('role_id',$request->role_id)->delete();
        }
        

    }
    
    public function get_api_aset(request $request){
        $data = ViewRole::where('active',1)->orderBy('nama_aset','Asc')->get();
        
        $response=array();
        foreach($data as $o){
            $response[]=array(
                'id'=>$o->id,
                'text'=> $o->nama_aset,
            );
        }
        return response()->json($response);
    }
    
    public function store(request $request){
        error_reporting(0);
        $rules = [];
        $messages = [];
        $rules['username']= 'required';
        $messages['username.required']= 'Masukan  NRK /Username ';
        $rules['name']= 'required';
        $messages['name.required']= 'Masukan  Nama Pengguna';
        $rules['email']= 'required|email';
        $messages['email.required']= 'Masukan  Email Addres';
        $messages['email.email']= 'Format email salah';
        $rules['role_id']= 'required|numeric';
        $messages['role_id.required']= 'Masukan  Role Otorisasi';
        $messages['role_id.numeric']= 'Masukan  Role Otorisasi';
        
        if($request->id==0){
            $rules['password']= 'required';
            $messages['password.required']= 'Masukan password';
        }
        
        
        
        
       
        $validator = Validator::make($request->all(), $rules, $messages);
        $val=$validator->Errors();


        if ($validator->fails()) {
            echo'<div class="nitof"><b>Oops Error !</b><br><div class="isi-nitof">';
                foreach(parsing_validator($val) as $value){
                    
                    foreach($value as $isi){
                        echo'-&nbsp;'.$isi.'<br>';
                    }
                }
            echo'</div></div>';
        }else{
            
            if($request->id==0){
                
                    
                    $data=Role::create([
                        'role'=>$request->role,
                        'active'=>1,
                    ]);

                    echo'@ok';
                
                    
                
            }else{
               
                    $data=Role::where('id',$request->id)->update([
                        'role'=>$request->role,
                        
                    ]);
                    
                    echo'@ok';
                
            }
        }
    }
    public function store_non(request $request){
        error_reporting(0);
        $rules = [];
        $messages = [];
        $rules['nama_aset']= 'required';
        $messages['nama_aset.required']= 'Masukan  Nama Aset';
        $rules['serial_number']= 'required';
        $messages['serial_number.required']= 'Masukan  No Serial';
        $rules['kategori_aset_id']= 'required';
        $messages['kategori_aset_id.required']= 'Pilih  Kategori Aset';
        $rules['jenis_aset_id']= 'required';
        $messages['jenis_aset_id.required']= 'Pilih  jenis Aset';
        $rules['satuan_id']= 'required';
        $messages['satuan_id.required']= 'Pilih  satuan Aset';
        $rules['sumber_aset_id']= 'required';
        $messages['sumber_aset_id.required']= 'Pilih  Asal Oleh / Sumber Aset';
        $rules['spesifikasi']= 'required';
        $messages['spesifikasi.required']= 'Masukan  spesifikasi Aset';
        
        
        // $rules['keterangan']= 'required';
        // $messages['keterangan.required']= 'Masukan nama keterangan';
        
        
       
        $validator = Validator::make($request->all(), $rules, $messages);
        $val=$validator->Errors();


        if ($validator->fails()) {
            echo'<div class="nitof"><b>Oops Error !</b><br><div class="isi-nitof">';
                foreach(parsing_validator($val) as $value){
                    
                    foreach($value as $isi){
                        echo'-&nbsp;'.$isi.'<br>';
                    }
                }
            echo'</div></div>';
        }else{
            
            if($request->id==0){
                
                    
                    $data=Role::create([
                        'nama_aset'=>$request->nama_aset,
                        'serial_number'=>$request->serial_number,
                        'kategori_aset_id'=>$request->kategori_aset_id,
                        'jenis_aset_id'=>$request->jenis_aset_id,
                        'satuan_id'=>$request->satuan_id,
                        'sumber_aset_id'=>$request->sumber_aset_id,
                        'spesifikasi'=>$request->spesifikasi,
                        'active'=>1,
                        'created_at'=>date('Y-m-d H:i:s'),
                    ]);

                    echo'@ok';
                
                    
                
            }else{
               
                    $data=Role::where('id',$request->id)->update([
                        'nama_aset'=>$request->nama_aset,
                        'serial_number'=>$request->serial_number,
                        'kategori_aset_id'=>$request->kategori_aset_id,
                        'jenis_aset_id'=>$request->jenis_aset_id,
                        'satuan_id'=>$request->satuan_id,
                        'sumber_aset_id'=>$request->sumber_aset_id,
                        'spesifikasi'=>$request->spesifikasi,
                        'updated_at'=>date('Y-m-d H:i:s'),
                        
                    ]);
                    
                    echo'@ok';
                
            }
        }
    }
    
}
