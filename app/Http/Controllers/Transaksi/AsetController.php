<?php

namespace App\Http\Controllers\Transaksi;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use GuzzleHttp\Client;
use Validator;
use App\Models\Kategori;
use App\Models\Merk;
use App\Models\Aset;
use App\Models\SatuanAset;
use App\Models\ViewAset;
class AsetController extends Controller
{
    
    public function index(request $request)
    {
        if(akses_modul(7)>0){
            $id=decoder($request->tipe);
            $data=Kategori::where('id',$id)->first();
            return view('transaksi.aset.index',compact('data'));
        }else{
            return view('error2');
        }
        //  $data=Aset::select('merk')->groupBy('merk')->get();
        //  foreach($data as $o){
        //     $save=Merk::Updateorcreate([
        //         'merk_aset'=>$o->merk,
        //         'active'=>1,
        //     ]);
        //  }
       

        // =======setting id merk
        //  $data=Aset::get();
        //  foreach($data as $o){
        //     $mst=Merk::where('merk_aset',$o->merk)->first();
        //     $save=Aset::where('id',$o->id)->update([
        //         'merk_aset_id'=>$mst->id,
        //         'active'=>1,
        //     ]);
        //  }

        // =======setting id satuan
        //  $data=Aset::get();
        //  foreach($data as $o){
        //     $mst=SatuanAset::where('satuan_aset',$o->satuan)->first();
        //     $save=Aset::where('id',$o->id)->update([
        //         'satuan_id'=>$mst->id,
        //         'active'=>1,
        //     ]);
        //  }


        // =======setting id tanggal
        //  $data=Aset::get();
        //  foreach($data as $o){
        //     $exp=explode('/',$o->tanggal_masuk);
        //     if(count($exp)==3){
        //         $tanggal=$exp[2].'-'.$exp[1].'-'.$exp[0];
        //         $save=Aset::where('id',$o->id)->update([
        //             'tanggal'=>$tanggal,
        //             'active'=>1,
        //         ]);
        //     }else{
        //         $tanggal=$o->tanggal_masuk;
        //         $save=Aset::where('id',$o->id)->update([
        //             'tanggal'=>$tanggal,
        //             'active'=>1,
        //         ]);
        //     }
            
        //  }
    }
    public function index_non(request $request)
    {
        if(Auth::user()->role_id==1){
            return view('transaksi.aset-non.index');
        }else{
            return view('error');
        }
         
       
        
    }
    
    public function view(request $request)
    {
        error_reporting(0);
        $template='top';
        $id=decoder($request->id);
        $data=Peminjaman::find($id);
        
        if($id>0){
            $disabled='readonly';
            
        }else{
            $disabled='readonly';
        }
        if(Auth::user()->role_id==1){
            return view('transaksi.aset.view',compact('template','data','disabled','id'));
        }else{
            return view('error');
        }
        
        
        
    }
    public function modal(request $request)
    {
        error_reporting(0);
        $template='top';
        $id=decoder($request->id);
        $mst=Kategori::find($request->kategori_id);
        $data=Aset::find($id);
        
        if($id>0){
            $disabled='readonly';
            
            
        }else{
            $disabled='readonly';
            
        }
        if(akses_modul(7)>0){
            if($mst->id==1){
                return view('transaksi.aset.modal_1',compact('template','data','disabled','id','mst'));
            }
            if($mst->id==2){
                return view('transaksi.aset.modal_2',compact('template','data','disabled','id','mst'));
            }
            if($mst->id==3){
                return view('transaksi.aset.modal_3',compact('template','data','disabled','id','mst'));
            }
            if($mst->id==4){
                return view('transaksi.aset.modal_4',compact('template','data','disabled','id','mst'));
            }
            
        }else{
            return view('error');
        }
        
        
        
    }
    public function modal_detail(request $request)
    {
        error_reporting(0);
        $template='top';
        $id=decoder($request->id);
        $mst=Kategori::find($request->kategori_id);
        $data=Aset::find($id);
        
        if($id>0){
            $disabled='readonly';
            
            
        }else{
            $disabled='readonly';
            
        }
        if(akses_modul(7)>0){
            if($mst->id==1){
                return view('transaksi.aset.modal_detail_1',compact('template','data','disabled','id','mst'));
            }
            if($mst->id==2){
                return view('transaksi.aset.modal_detail_2',compact('template','data','disabled','id','mst'));
            }
            if($mst->id==3){
                return view('transaksi.aset.modal_detail_3',compact('template','data','disabled','id','mst'));
            }
            if($mst->id==4){
                return view('transaksi.aset.modal_detail_4',compact('template','data','disabled','id','mst'));
            }
            
        }else{
            return view('error');
        }
        
        
        
    }
    public function cek_aset(request $request,$id)
    {
        error_reporting(0);
        $template='top';
        $id=decoder($request->id);
        $mst=Kategori::find($request->kategori_id);
        $data=Aset::find($id);
        return view('cekaset',compact('template','data','disabled','id','mst'));
        
        
        
    }
    public function modal_non(request $request)
    {
        error_reporting(0);
        $template='top';
        $id=decoder($request->id);
        $data=Aset::find($id);
        
        if($id>0){
            $disabled='readonly';
            
            
        }else{
            $disabled='readonly';
            
        }
        if(Auth::user()->role_id==1){
            return view('transaksi.aset-non.modal',compact('template','data','disabled','id'));
        }else{
            return view('error');
        }
        
        
        
    }
    
    public function get_data(request $request)
    {
        error_reporting(0);
        $data = ViewAset::whereIn('active',array(1,0))->where('kategori_aset_id',$request->kategori_id)->orderBy('nama_aset','Asc')->get();
        // $data = ViewAset::whereIn('active',array(1,0))->orderBy('nama_aset','Asc')->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn='
                <div class="btn-group btn-group-sm ">
                    <a href="#" data-toggle="dropdown" class="btn btn-success btn-xs dropdown-toggle" title="Pilih proses"><i class="fas fa-cog fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="javascript:;" class="dropdown-item" onclick="tambah(`'.encoder($row->id).'`)"><i class="fas fa-pencil-alt fa-fw"></i> Ubah</a>
                        <div class="dropdown-divider"></div>
                        <a href="javascript:;" class="dropdown-item" onclick="delete_data(`'.encoder($row->id).'`)"><i class="fas fa-trash-alt fa-fw"></i> Hapus</a>
                        <a href="javascript:;" class="dropdown-item" onclick="detail_data(`'.encoder($row->id).'`)"><i class="fas fa-search fa-fw"></i> Detail</a>
                    </div>
                </div>
                ';
                return $btn;
            })
            ->addColumn('act', function ($row) {
                $btn='<input type="checkbox" name="nik[]" value="'.$row->nik.'">';
                
                return $btn;
            })
            ->addColumn('harga', function ($row) {
                
                return uang($row->harga);
            })
            ->addColumn('status', function ($row) {
                if($row->active==1){
                    $btn='<div class="custom-control custom-switch mb-1">
                        <input type="checkbox" class="custom-control-input" onclick="switch_data('.$row->id.',0)" id="customSwitch'.$row->id.'" checked>
                        <label class="custom-control-label" for="customSwitch'.$row->id.'"></label>
                    </div>';
                }else{
                    $btn='<div class="custom-control custom-switch mb-1">
                        <input type="checkbox" class="custom-control-input" onclick="switch_data('.$row->id.',1)" id="customSwitch'.$row->id.'" >
                        <label class="custom-control-label" for="customSwitch'.$row->id.'"></label>
                    </div>';
                }
                
                
                return $btn;
            })
           
            
            ->rawColumns(['action','act','status'])
            ->make(true);
    }
    public function get_data_non(request $request)
    {
        error_reporting(0);
        $data = ViewAset::whereIn('active',array(1,0))->where('kategori_aset_id','!=',2)->orderBy('nama_aset','Asc')->get();
        // $data = ViewAset::whereIn('active',array(1,0))->orderBy('nama_aset','Asc')->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn='
                <div class="btn-group btn-group-sm ">
                    <a href="#" data-toggle="dropdown" class="btn btn-success btn-xs dropdown-toggle" title="Pilih proses"><i class="fas fa-cog fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="javascript:;" class="dropdown-item" onclick="tambah(`'.encoder($row->id).'`)"><i class="fas fa-pencil-alt fa-fw"></i> Ubah</a>
                        <div class="dropdown-divider"></div>
                        <a href="javascript:;" class="dropdown-item" onclick="delete_data(`'.encoder($row->id).'`)"><i class="fas fa-trash-alt fa-fw"></i> Hapus</a>
                    </div>
                </div>
                ';
                return $btn;
            })
            ->addColumn('act', function ($row) {
                $btn='<input type="checkbox" name="nik[]" value="'.$row->nik.'">';
                
                return $btn;
            })
            ->addColumn('harga', function ($row) {
                
                return uang($row->harga);
            })
            ->addColumn('status', function ($row) {
                if($row->active==1){
                    $btn='<div class="custom-control custom-switch mb-1">
                        <input type="checkbox" class="custom-control-input" onclick="switch_data('.$row->id.',0)" id="customSwitch'.$row->id.'" checked>
                        <label class="custom-control-label" for="customSwitch'.$row->id.'"></label>
                    </div>';
                }else{
                    $btn='<div class="custom-control custom-switch mb-1">
                        <input type="checkbox" class="custom-control-input" onclick="switch_data('.$row->id.',1)" id="customSwitch'.$row->id.'" >
                        <label class="custom-control-label" for="customSwitch'.$row->id.'"></label>
                    </div>';
                }
                
                
                return $btn;
            })
           
            
            ->rawColumns(['action','act','status'])
            ->make(true);
    }
    public function get_data_aset(request $request)
    {
        error_reporting(0);
        $data = ViewAset::whereIn('active',array(1,0))->orderBy('nama_aset','Asc')->get();
        // $data = ViewAset::whereIn('active',array(1,0))->orderBy('nama_aset','Asc')->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn='<span class="btn btn-xs btn-primary" onclick="pilih_aset('.$row->id.',`0`)">Pilih</span>';
                return $btn;
            })
            ->addColumn('act', function ($row) {
                $btn='<input type="checkbox" name="nik[]" value="'.$row->nik.'">';
                
                return $btn;
            })
            ->addColumn('qris', function ($row) {
                $btn=barcoderr(encoder($row->id));
                
                return $btn;
            })
            ->addColumn('harga', function ($row) {
                
                return uang($row->harga);
            })
            ->addColumn('status', function ($row) {
                if($row->active==1){
                    $btn='<div class="custom-control custom-switch mb-1">
                        <input type="checkbox" class="custom-control-input" onclick="switch_data('.$row->id.',0)" id="customSwitch'.$row->id.'" checked>
                        <label class="custom-control-label" for="customSwitch'.$row->id.'"></label>
                    </div>';
                }else{
                    $btn='<div class="custom-control custom-switch mb-1">
                        <input type="checkbox" class="custom-control-input" onclick="switch_data('.$row->id.',1)" id="customSwitch'.$row->id.'" >
                        <label class="custom-control-label" for="customSwitch'.$row->id.'"></label>
                    </div>';
                }
                
                
                return $btn;
            })
           
            
            ->rawColumns(['action','act','status','qris'])
            ->make(true);
    }
    
    
    public function delete_data(request $request){
        if(Auth::user()->role_id==1 || Auth::user()->role_id==4){
            $id=decoder($request->id);
            $data = Aset::where('id',$id)->update(['active'=>2]);
        }
        

    }
    public function switch_status(request $request){
        if(Auth::user()->role_id==1 || Auth::user()->role_id==4){
            $data = Aset::where('id',$request->id)->update(['active'=>$request->act]);
        }
        

    }
    
    public function get_api_aset(request $request){
        $data = ViewAset::where('active',1)->orderBy('nama_aset','Asc')->get();
        
        $response=array();
        foreach($data as $o){
            $response[]=array(
                'id'=>$o->id,
                'text'=> $o->nama_aset,
            );
        }
        return response()->json($response);
    }
    
    public function store(request $request){
        error_reporting(0);
        $rules = [];
        $messages = [];
        $rules['kode_aset']= 'required';
        $messages['kode_aset.required']= 'Masukan  Nama Aset';
        $rules['nama_aset']= 'required';
        $messages['nama_aset.required']= 'Masukan  Nama Aset';
        $rules['kategori_aset_id']= 'required';
        $messages['kategori_aset_id.required']= 'Pilih  Kategori Aset';
        $rules['jenis_aset_id']= 'required';
        $messages['jenis_aset_id.required']= 'Pilih  jenis Aset';
        $rules['satuan_id']= 'required';
        $messages['satuan_id.required']= 'Pilih  satuan Aset';
        $rules['sumber_aset_id']= 'required';
        $messages['sumber_aset_id.required']= 'Pilih  Asal Oleh / Sumber Aset';
        $rules['tanggal']= 'required';
        $messages['tanggal.required']= 'Pilih  Tanggal Masuk Aset';
        $rules['harga']= 'required';
        $messages['harga.required']= 'Masukan  Harga Aset';
        
        
        // $rules['keterangan']= 'required';
        // $messages['keterangan.required']= 'Masukan nama keterangan';
        
        
       
        $validator = Validator::make($request->all(), $rules, $messages);
        $val=$validator->Errors();


        if ($validator->fails()) {
            echo'<div class="nitof"><b>Oops Error !</b><br><div class="isi-nitof">';
                foreach(parsing_validator($val) as $value){
                    
                    foreach($value as $isi){
                        echo'-&nbsp;'.$isi.'<br>';
                    }
                }
            echo'</div></div>';
        }else{
            
            if($request->id==0){
                    $merk=Merk::find($request->merk_aset_id);
                    $satuan=SatuanAset::find($request->satuan_id);
                    
                    $data=Aset::create([
                        'kode_aset'=>$request->kode_aset,
                        'merk'=>$request->merk,
                        'tipe'=>$request->tipe,
                        'satuan'=>$satuan->satuan_aset,
                        'harga'=>$request->harga,
                        'nama_aset'=>$request->nama_aset,
                        'serial_number'=>$request->serial_number,
                        'kategori_aset_id'=>$request->kategori_aset_id,
                        'jenis_aset_id'=>$request->jenis_aset_id,
                        'satuan_id'=>$request->satuan_id,
                        'sumber_aset_id'=>$request->sumber_aset_id,
                        'tanggal'=>$request->tanggal,
                        'tipe'=>$request->spesifikasi,
                        'active'=>1,
                        'created_at'=>date('Y-m-d H:i:s'),
                    ]);

                    echo'@ok';
                
                    
                
            }else{
                    $merk=Merk::find($request->merk_aset_id);
                    $satuan=SatuanAset::find($request->satuan_id);
                    $data=Aset::where('id',$request->id)->update([
                        'kode_aset'=>$request->kode_aset,
                        'merk'=>$request->merk,
                        'satuan'=>$satuan->satuan_aset,
                        'nama_aset'=>$request->nama_aset,
                        'harga'=>$request->harga,
                        'serial_number'=>$request->serial_number,
                        'kategori_aset_id'=>$request->kategori_aset_id,
                        'jenis_aset_id'=>$request->jenis_aset_id,
                        'satuan_id'=>$request->satuan_id,
                        'tipe'=>$request->tipe,
                        'sumber_aset_id'=>$request->sumber_aset_id,
                        'tanggal'=>$request->tanggal,
                        'tipe'=>$request->spesifikasi,
                        'updated_at'=>date('Y-m-d H:i:s'),
                        
                    ]);
                    
                    echo'@ok';
                
            }
        }
    }
    public function store_non(request $request){
        error_reporting(0);
        $rules = [];
        $messages = [];
        $rules['nama_aset']= 'required';
        $messages['nama_aset.required']= 'Masukan  Nama Aset';
        $rules['serial_number']= 'required';
        $messages['serial_number.required']= 'Masukan  No Serial';
        $rules['kategori_aset_id']= 'required';
        $messages['kategori_aset_id.required']= 'Pilih  Kategori Aset';
        $rules['jenis_aset_id']= 'required';
        $messages['jenis_aset_id.required']= 'Pilih  jenis Aset';
        $rules['satuan_id']= 'required';
        $messages['satuan_id.required']= 'Pilih  satuan Aset';
        $rules['sumber_aset_id']= 'required';
        $messages['sumber_aset_id.required']= 'Pilih  Asal Oleh / Sumber Aset';
        $rules['spesifikasi']= 'required';
        $messages['spesifikasi.required']= 'Masukan  spesifikasi Aset';
        
        
        // $rules['keterangan']= 'required';
        // $messages['keterangan.required']= 'Masukan nama keterangan';
        
        
       
        $validator = Validator::make($request->all(), $rules, $messages);
        $val=$validator->Errors();


        if ($validator->fails()) {
            echo'<div class="nitof"><b>Oops Error !</b><br><div class="isi-nitof">';
                foreach(parsing_validator($val) as $value){
                    
                    foreach($value as $isi){
                        echo'-&nbsp;'.$isi.'<br>';
                    }
                }
            echo'</div></div>';
        }else{
            
            if($request->id==0){
                
                    
                    $data=Aset::create([
                        'nama_aset'=>$request->nama_aset,
                        'serial_number'=>$request->serial_number,
                        'kategori_aset_id'=>$request->kategori_aset_id,
                        'jenis_aset_id'=>$request->jenis_aset_id,
                        'satuan_id'=>$request->satuan_id,
                        'sumber_aset_id'=>$request->sumber_aset_id,
                        'spesifikasi'=>$request->spesifikasi,
                        'active'=>1,
                        'created_at'=>date('Y-m-d H:i:s'),
                    ]);

                    echo'@ok';
                
                    
                
            }else{
               
                    $data=Aset::where('id',$request->id)->update([
                        'nama_aset'=>$request->nama_aset,
                        'serial_number'=>$request->serial_number,
                        'kategori_aset_id'=>$request->kategori_aset_id,
                        'jenis_aset_id'=>$request->jenis_aset_id,
                        'satuan_id'=>$request->satuan_id,
                        'sumber_aset_id'=>$request->sumber_aset_id,
                        'spesifikasi'=>$request->spesifikasi,
                        'updated_at'=>date('Y-m-d H:i:s'),
                        
                    ]);
                    
                    echo'@ok';
                
            }
        }
    }
    
}
