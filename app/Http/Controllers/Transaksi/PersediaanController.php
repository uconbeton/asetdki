<?php

namespace App\Http\Controllers\Transaksi;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use GuzzleHttp\Client;
use Validator;
use App\Models\Persediaan;
use App\Models\ViewPersediaan;
use App\Models\ViewPeminjaman;
use App\Models\ViewKategori;
use App\Models\Aset;
use App\Models\ViewAset;
use App\Models\Stok;
use App\Models\ViewStok;
class PersediaanController extends Controller
{
    
    public function index(request $request)
    {
        if(Auth::user()->role_id==1){
            return view('transaksi.persediaan.index');
        }else{
            return view('error');
        }
         
       
        
    }
    
    public function view(request $request)
    {
        error_reporting(0);
        $template='top';
        $id=decoder($request->id);
        $data=Persediaan::find($id);
        
        if($id>0){
            $disabled='readonly';
            
        }else{
            $disabled='readonly';
        }
        if(Auth::user()->role_id==1){
            return view('transaksi.persediaan.view',compact('template','data','disabled','id'));
        }else{
            return view('error');
        }
        
        
        
    }
    public function modal(request $request)
    {
        error_reporting(0);
        $template='top';
        $id=$request->id;
        $qty=$request->qty;
        $persediaan_id=$request->persediaan_id;
        $data=ViewAset::find($id);
        
        if($id>0){
            $disabled='readonly';
            
            
        }else{
            $disabled='readonly';
            
        }
        if(Auth::user()->role_id==1){
            return view('transaksi.persediaan.modal',compact('template','data','disabled','id','qty','persediaan_id'));
        }else{
            return view('error');
        }
        
        
        
    }
    
    public function get_data(request $request)
    {
        error_reporting(0);
        $data = ViewPersediaan::whereIn('active',array(1,0))->orderBy('id','Asc')->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn='
                <div class="btn-group btn-group-sm ">
                    <a href="#" data-toggle="dropdown" class="btn btn-success btn-xs dropdown-toggle" title="Pilih proses"><i class="fas fa-cog fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="javascript:;" class="dropdown-item" onclick="tambah(`'.encoder($row->id).'`)"><i class="fas fa-pencil-alt fa-fw"></i> Ubah</a>
                        <div class="dropdown-divider"></div>
                        <a href="javascript:;" class="dropdown-item" onclick="delete_data(`'.encoder($row->id).'`)"><i class="fas fa-trash-alt fa-fw"></i> Hapus</a>
                    </div>
                </div>
                ';
                return $btn;
            })
            ->addColumn('pengajuan', function ($row) {
                $btn=$row->nrk.' '.$row->nama_pengajuan;
                
                return $btn;
            })
            ->addColumn('penyedia', function ($row) {
                $btn=$row->nama_penyedia;
                
                return $btn;
            })
            ->addColumn('harga', function ($row) {
                
                return uang($row->harga);
            })
            ->addColumn('status', function ($row) {
                if($row->active==1){
                    $btn='<div class="custom-control custom-switch mb-1">
                        <input type="checkbox" class="custom-control-input" onclick="switch_data('.$row->id.',0)" id="customSwitch'.$row->id.'" checked>
                        <label class="custom-control-label" for="customSwitch'.$row->id.'"></label>
                    </div>';
                }else{
                    $btn='<div class="custom-control custom-switch mb-1">
                        <input type="checkbox" class="custom-control-input" onclick="switch_data('.$row->id.',1)" id="customSwitch'.$row->id.'" >
                        <label class="custom-control-label" for="customSwitch'.$row->id.'"></label>
                    </div>';
                }
                
                
                return $btn;
            })
           
            
            ->rawColumns(['action','act','status'])
            ->make(true);
    }
    public function get_data_detail(request $request)
    {
        error_reporting(0);
        $data = ViewStok::where('peminjaman_id',$request->peminjaman_id)->orderBy('id','Asc')->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn='
                <div class="btn-group btn-group-sm ">
                    <a href="#" data-toggle="dropdown" class="btn btn-success btn-xs dropdown-toggle" title="Pilih proses"><i class="fas fa-cog fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="javascript:;" class="dropdown-item" onclick="pilih_aset('.$row->aset_id.','.$row->qty.')"><i class="fas fa-pencil-alt fa-fw"></i> Ubah</a>
                        <div class="dropdown-divider"></div>
                        <a href="javascript:;" class="dropdown-item" onclick="delete_data(`'.encoder($row->id).'`)"><i class="fas fa-trash-alt fa-fw"></i> Hapus</a>
                    </div>
                </div>
                ';
                return $btn;
            })
            ->addColumn('act', function ($row) {
                $btn='<input type="checkbox" name="nik[]" value="'.$row->nik.'">';
                
                return $btn;
            })
            ->addColumn('harga', function ($row) {
                
                return uang($row->harga);
            })
            ->addColumn('status', function ($row) {
                if($row->active==1){
                    $btn='<div class="custom-control custom-switch mb-1">
                        <input type="checkbox" class="custom-control-input" onclick="switch_data('.$row->id.',0)" id="customSwitch'.$row->id.'" checked>
                        <label class="custom-control-label" for="customSwitch'.$row->id.'"></label>
                    </div>';
                }else{
                    $btn='<div class="custom-control custom-switch mb-1">
                        <input type="checkbox" class="custom-control-input" onclick="switch_data('.$row->id.',1)" id="customSwitch'.$row->id.'" >
                        <label class="custom-control-label" for="customSwitch'.$row->id.'"></label>
                    </div>';
                }
                
                
                return $btn;
            })
           
            
            ->rawColumns(['action','act','status'])
            ->make(true);
    }
    
    
    public function delete_data(request $request){
        if(Auth::user()->role_id==1 || Auth::user()->role_id==4){
            $id=decoder($request->id);
            $data = Persediaan::where('id',$id)->update(['active'=>2]);
        }
        

    }
    public function switch_status(request $request){
        if(Auth::user()->role_id==1 || Auth::user()->role_id==4){
            $data = Persediaan::where('id',$request->id)->update(['active'=>$request->act]);
        }
        

    }
    
    public function store(request $request){
        error_reporting(0);
        $rules = [];
        $messages = [];
        $rules['nrk']= 'required';
        $messages['nrk.required']= 'Masukan  NRK Yang Mengajukan';
        $rules['nama_pengajuan']= 'required';
        $messages['nama_pengajuan.required']= 'Masukan  Nama Yang Mengajukan';
        $rules['bidang_id']= 'required';
        $messages['bidang_id.required']= 'Pilih  Bidang / OPD';
        $rules['tanggal']= 'required';
        $messages['tanggal.required']= 'Masukan  Tanggal Pengajuan';
        
        $rules['alasan_pengajuan']= 'required';
        $messages['alasan_pengajuan.required']= 'Masukan alasan pengajuan';
        
        $rules['nama_penyedia']= 'required';
        $messages['nama_penyedia.required']= 'Masukan  Nama Penyedia';
        $rules['no_telepon']= 'required';
        $messages['no_telepon.required']= 'Masukan  No Telepon Penyedia';
        $rules['alamat_penyedia']= 'required';
        $messages['alamat_penyedia.required']= 'Masukan  Alamat Penyedia';
       
        $validator = Validator::make($request->all(), $rules, $messages);
        $val=$validator->Errors();


        if ($validator->fails()) {
            echo'<div class="nitof"><b>Oops Error !</b><br><div class="isi-nitof">';
                foreach(parsing_validator($val) as $value){
                    
                    foreach($value as $isi){
                        echo'-&nbsp;'.$isi.'<br>';
                    }
                }
            echo'</div></div>';
        }else{
            
            if($request->id==0){
                
                    
                    $data=Persediaan::create([
                        'nomor'=>penomoran_pengajuan(),
                        'nrk'=>$request->nrk,
                        'nama_pengajuan'=>$request->nama_pengajuan,
                        'bidang_id'=>$request->bidang_id,
                        'tanggal'=>$request->tanggal,
                        'alasan_pengajuan'=>$request->alasan_pengajuan,
                        'nama_penyedia'=>$request->nama_penyedia,
                        'alamat_penyedia'=>$request->alamat_penyedia,
                        'no_telepon'=>$request->alasan_pengajuan,
                        'active'=>1,
                        'status'=>$request->act,
                        'created_at'=>date('Y-m-d H:i:s'),
                    ]);
                    if($request->act==0){
                        echo'@ok@'.encoder($data->id).'@1@';
                    }else{
                        echo'@ok@';
                    }
                    
                
                    
                
            }else{
               
                    $data=Persediaan::where('id',$request->id)->update([
                        'nrk'=>$request->nrk,
                        'nama_peminjam'=>$request->nama_peminjam,
                        'bidang_id'=>$request->bidang_id,
                        'tanggal'=>$request->tanggal,
                        'alasan_pinjam'=>$request->alasan_pinjam,
                        'status'=>1,
                        'updated_at'=>date('Y-m-d H:i:s'),
                        
                    ]);
                    
                    echo'@ok';
                
            }
        }
    }
    public function store_pengajuan(request $request){
        error_reporting(0);
        $rules = [];
        $messages = [];
        $rules['aset_id']= 'required';
        $messages['aset_id.required']= 'Pilih Aset ';
        $rules['qty']= 'required|numeric';
        $messages['qty.required']= 'Masukan  jumlah Qty';
        
       
        $validator = Validator::make($request->all(), $rules, $messages);
        $val=$validator->Errors();


        if ($validator->fails()) {
            echo'<div class="nitof"><b>Oops Error !</b><br><div class="isi-nitof">';
                foreach(parsing_validator($val) as $value){
                    
                    foreach($value as $isi){
                        echo'-&nbsp;'.$isi.'<br>';
                    }
                }
            echo'</div></div>';
        }else{
            
            if($request->id==0){
                
                    
                    $data=Stok::UpdateOrcreate([
                        'aset_id'=>$request->aset_id,
                        'peminjaman_id'=>$request->persediaan_id,
                    ],[
                        'sts'=>2,
                        'qty'=>$request->qty,
                        'active'=>1,
                        'created_at'=>date('Y-m-d H:i:s'),
                    ]);
                    
                    echo'@ok@';
                    
                    
                
                    
                
            }else{
               
                    $data=Stok::UpdateOrcreate([
                        'aset_id'=>$request->aset_id,
                        'peminjaman_id'=>$request->peminjaman_id,
                    ],[
                        'sts'=>2,
                        'qty'=>$request->qty,
                        'active'=>1,
                        'created_at'=>date('Y-m-d H:i:s'),
                    ]);
                    
                    echo'@ok';
                
            }
        }
    }
    
}
