<?php

namespace App\Http\Controllers\Transaksi;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use GuzzleHttp\Client;
use Validator;
use App\Models\Peminjaman;
use App\Models\ViewPeminjaman;
use App\Models\ViewKategori;
use App\Models\Aset;
use App\Models\ViewAset;
use App\Models\Stok;
use App\Models\ViewStok;
class PeminjamanController extends Controller
{
    
    public function index(request $request)
    {
        if(akses_modul(8)>0){
            return view('transaksi.peminjaman.index');
        }else{
            return view('error');
        }
         
       
        
    }
    
    public function view(request $request)
    {
        error_reporting(0);
        $template='top';
        $id=decoder($request->id);
        $data=Peminjaman::find($id);
        
        if($id>0){
            $disabled='readonly';
            
        }else{
            $disabled='readonly';
        }
        if(Auth::user()->role_id==1){
            return view('transaksi.peminjaman.view',compact('template','data','disabled','id'));
        }else{
            return view('error');
        }
        
        
        
    }
    public function modal(request $request)
    {
        error_reporting(0);
        $template='top';
        $id=$request->id;
        $qty=$request->qty;
        $peminjaman_id=$request->peminjaman_id;
        $data=ViewAset::find($id);
        
        if($id>0){
            $disabled='readonly';
            
            
        }else{
            $disabled='readonly';
            
        }
        if(Auth::user()->role_id==1){
            return view('transaksi.peminjaman.modal',compact('template','data','disabled','id','qty','peminjaman_id'));
        }else{
            return view('error');
        }
        
        
        
    }
    
    public function get_data(request $request)
    {
        error_reporting(0);
        $data = ViewPeminjaman::whereIn('active',array(1,0))->orderBy('id','Asc')->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn='
                <div class="btn-group btn-group-sm ">
                    <a href="#" data-toggle="dropdown" class="btn btn-success btn-xs dropdown-toggle" title="Pilih proses"><i class="fas fa-cog fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="javascript:;" class="dropdown-item" onclick="tambah(`'.encoder($row->id).'`)"><i class="fas fa-pencil-alt fa-fw"></i> Ubah</a>
                        <div class="dropdown-divider"></div>
                        <a href="javascript:;" class="dropdown-item" onclick="delete_data(`'.encoder($row->id).'`)"><i class="fas fa-trash-alt fa-fw"></i> Hapus</a>
                    </div>
                </div>
                ';
                return $btn;
            })
            ->addColumn('act', function ($row) {
                $btn='<input type="checkbox" name="nik[]" value="'.$row->nik.'">';
                
                return $btn;
            })
            ->addColumn('harga', function ($row) {
                
                return uang($row->harga);
            })
            ->addColumn('status', function ($row) {
                if($row->active==1){
                    $btn='<div class="custom-control custom-switch mb-1">
                        <input type="checkbox" class="custom-control-input" onclick="switch_data('.$row->id.',0)" id="customSwitch'.$row->id.'" checked>
                        <label class="custom-control-label" for="customSwitch'.$row->id.'"></label>
                    </div>';
                }else{
                    $btn='<div class="custom-control custom-switch mb-1">
                        <input type="checkbox" class="custom-control-input" onclick="switch_data('.$row->id.',1)" id="customSwitch'.$row->id.'" >
                        <label class="custom-control-label" for="customSwitch'.$row->id.'"></label>
                    </div>';
                }
                
                
                return $btn;
            })
           
            
            ->rawColumns(['action','act','status'])
            ->make(true);
    }
    public function get_data_detail(request $request)
    {
        error_reporting(0);
        $data = ViewStok::where('peminjaman_id',$request->peminjaman_id)->orderBy('id','Asc')->get();

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn='
                <div class="btn-group btn-group-sm ">
                    <a href="#" data-toggle="dropdown" class="btn btn-success btn-xs dropdown-toggle" title="Pilih proses"><i class="fas fa-cog fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="javascript:;" class="dropdown-item" onclick="pilih_aset('.$row->aset_id.','.$row->qty.')"><i class="fas fa-pencil-alt fa-fw"></i> Ubah</a>
                        <div class="dropdown-divider"></div>
                        <a href="javascript:;" class="dropdown-item" onclick="delete_data(`'.encoder($row->id).'`)"><i class="fas fa-trash-alt fa-fw"></i> Hapus</a>
                    </div>
                </div>
                ';
                return $btn;
            })
            ->addColumn('act', function ($row) {
                $btn='<input type="checkbox" name="nik[]" value="'.$row->nik.'">';
                
                return $btn;
            })
            ->addColumn('harga', function ($row) {
                
                return uang($row->harga);
            })
            ->addColumn('status', function ($row) {
                if($row->active==1){
                    $btn='<div class="custom-control custom-switch mb-1">
                        <input type="checkbox" class="custom-control-input" onclick="switch_data('.$row->id.',0)" id="customSwitch'.$row->id.'" checked>
                        <label class="custom-control-label" for="customSwitch'.$row->id.'"></label>
                    </div>';
                }else{
                    $btn='<div class="custom-control custom-switch mb-1">
                        <input type="checkbox" class="custom-control-input" onclick="switch_data('.$row->id.',1)" id="customSwitch'.$row->id.'" >
                        <label class="custom-control-label" for="customSwitch'.$row->id.'"></label>
                    </div>';
                }
                
                
                return $btn;
            })
           
            
            ->rawColumns(['action','act','status'])
            ->make(true);
    }
    
    
    public function delete_data(request $request){
        if(Auth::user()->role_id==1 || Auth::user()->role_id==4){
            $id=decoder($request->id);
            $data = Peminjaman::where('id',$id)->update(['active'=>2]);
        }
        

    }
    public function switch_status(request $request){
        if(Auth::user()->role_id==1 || Auth::user()->role_id==4){
            $data = Peminjaman::where('id',$request->id)->update(['active'=>$request->act]);
        }
        

    }
    
    public function store(request $request){
        error_reporting(0);
        $rules = [];
        $messages = [];
        $rules['nrk']= 'required';
        $messages['nrk.required']= 'Masukan  NRK Peminjam';
        $rules['nama_peminjam']= 'required';
        $messages['nama_peminjam.required']= 'Masukan  Nama Peminjam';
        $rules['bidang_id']= 'required';
        $messages['bidang_id.required']= 'Pilih  Bidang / OPD';
        $rules['tanggal']= 'required';
        $messages['tanggal.required']= 'Masukan  Tanggal Peminjaman';
        $rules['tanggal_sampai']= 'required';
        $messages['tanggal_sampai.required']= 'Masukan  Tanggal pengembalian';
        
        $rules['alasan_pinjam']= 'required';
        $messages['alasan_pinjam.required']= 'Masukan alasan permintaan';
        
        
        if($request->bidang_id==14){
            $rules['keterangan_peminjam']= 'required';
            $messages['keterangan_peminjam.required']= 'Masukan keterangan peminjam';
        }
        $validator = Validator::make($request->all(), $rules, $messages);
        $val=$validator->Errors();


        if ($validator->fails()) {
            echo'<div class="nitof"><b>Oops Error !</b><br><div class="isi-nitof">';
                foreach(parsing_validator($val) as $value){
                    
                    foreach($value as $isi){
                        echo'-&nbsp;'.$isi.'<br>';
                    }
                }
            echo'</div></div>';
        }else{
            
            if($request->id==0){
                
                    
                    $data=Peminjaman::create([
                        'nomor'=>penomoran_peminjaman(),
                        'nrk'=>$request->nrk,
                        'nama_peminjam'=>$request->nama_peminjam,
                        'bidang_id'=>$request->bidang_id,
                        'tanggal'=>$request->tanggal,
                        'tanggal_sampai'=>$request->tanggal_sampai,
                        'keterangan_peminjam'=>$request->keterangan_peminjam,
                        'alasan_pinjam'=>$request->alasan_pinjam,
                        'active'=>1,
                        'status'=>$request->act,
                        'created_at'=>date('Y-m-d H:i:s'),
                    ]);
                    if($request->act==0){
                        echo'@ok@'.encoder($data->id).'@1@';
                    }else{
                        echo'@ok@';
                    }
                    
                
                    
                
            }else{
               
                    $data=Peminjaman::where('id',$request->id)->update([
                        'nrk'=>$request->nrk,
                        'nama_peminjam'=>$request->nama_peminjam,
                        'bidang_id'=>$request->bidang_id,
                        'tanggal_sampai'=>$request->tanggal_sampai,
                        'keterangan_peminjam'=>$request->keterangan_peminjam,
                        'tanggal'=>$request->tanggal,
                        'alasan_pinjam'=>$request->alasan_pinjam,
                        'status'=>1,
                        'updated_at'=>date('Y-m-d H:i:s'),
                        
                    ]);
                    
                    echo'@ok';
                
            }
        }
    }
    public function store_peminjaman(request $request){
        error_reporting(0);
        $rules = [];
        $messages = [];
        $rules['aset_id']= 'required';
        $messages['aset_id.required']= 'Pilih Aset ';
        $rules['qty']= 'required|numeric';
        $messages['qty.required']= 'Masukan  jumlah Qty';
        
       
        $validator = Validator::make($request->all(), $rules, $messages);
        $val=$validator->Errors();


        if ($validator->fails()) {
            echo'<div class="nitof"><b>Oops Error !</b><br><div class="isi-nitof">';
                foreach(parsing_validator($val) as $value){
                    
                    foreach($value as $isi){
                        echo'-&nbsp;'.$isi.'<br>';
                    }
                }
            echo'</div></div>';
        }else{
            
            if($request->id==0){
                
                    
                    $data=Stok::UpdateOrcreate([
                        'aset_id'=>$request->aset_id,
                        'peminjaman_id'=>$request->peminjaman_id,
                    ],[
                        'sts'=>2,
                        'qty'=>$request->qty,
                        'active'=>1,
                        'created_at'=>date('Y-m-d H:i:s'),
                    ]);
                    
                    echo'@ok@';
                    
                    
                
                    
                
            }else{
               
                    $data=Stok::UpdateOrcreate([
                        'aset_id'=>$request->aset_id,
                        'peminjaman_id'=>$request->peminjaman_id,
                    ],[
                        'sts'=>2,
                        'qty'=>$request->qty,
                        'active'=>1,
                        'created_at'=>date('Y-m-d H:i:s'),
                    ]);
                    
                    echo'@ok';
                
            }
        }
    }
    
}
