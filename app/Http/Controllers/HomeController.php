<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ViewDashboard;
use App\Models\Peminjaman;
use App\Models\Persediaan;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(request $request)
    {
        return view('home');
    }
    public function get_api_aset(request $request){
        $data = ViewDashboard::orderBy('nama_aset','Asc')->get();
        
        $response=array();
        foreach($data as $o){
            $response[]=array(
                'id'=>$o->id,
                'text'=> $o->nama_aset,
            );
        }
        return response()->json($response);
    }
    public function get_data_dashboard(request $request)
    {
        error_reporting(0);
        
        $data = ViewDashboard::where('id','!=',5)->orderBy('id','Asc')->get();
        
        $response=array();
        foreach($data as $o){
            $response[]=array(
                'id'=>$o->id,
                'kategori'=> $o->kategori,
                'total'=> $o->total,
                'color'=> $o->color,
            );
        }
        return response()->json($response);
    }
    public function get_data_dashboard_pengguna(request $request)
    {
        error_reporting(0);
        
        // $data = ViewDashboard::orderBy('id','Asc')->get();
        
        $response=array();
        $tahun=date('Y');
        for($x=1;$x<=12;$x++){
            
            $peminjaman=Peminjaman::whereMonth('tanggal',ubah_bulan($x))->whereYear('tanggal',$tahun)->count();
            $persediaan=Persediaan::whereMonth('tanggal',ubah_bulan($x))->whereYear('tanggal',$tahun)->count();
            $response[]=array(
                'bln'=>ubah_bulan($x),
                'bulan'=>bulan(ubah_bulan($x)),
                'peminjaman'=>$peminjaman,
                'persediaan'=> $persediaan,
            );
            
        }
        return response()->json($response);
    }
}
