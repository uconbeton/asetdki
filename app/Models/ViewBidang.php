<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewBidang extends Model
{
    use HasFactory;
    protected $table = 'view_organisasi';
    protected $guarded = ['id'];
    public $timestamps = false;
    
}
