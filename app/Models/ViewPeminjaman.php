<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewPeminjaman extends Model
{
    use HasFactory;
    protected $table = 'view_peminjaman';
    protected $guarded = ['id'];
    public $timestamps = false;
    
}
