<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BahanAset extends Model
{
    use HasFactory;
    protected $table = 'bahan';
    protected $guarded = ['id'];
    public $timestamps = false;
    
}
