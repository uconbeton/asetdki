<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SumberAset extends Model
{
    use HasFactory;
    protected $table = 'sumber_aset';
    protected $guarded = ['id'];
    public $timestamps = false;
    
}
