<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Akses extends Model
{
    use HasFactory;
    protected $table = 'akses_modul';
    protected $guarded = ['id'];
    public $timestamps = false;
    
}
