<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewAkses extends Model
{
    use HasFactory;
    protected $table = 'view_akses_modul';
    protected $guarded = ['id'];
    public $timestamps = false;
    
}
