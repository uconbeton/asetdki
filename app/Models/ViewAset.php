<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewAset extends Model
{
    use HasFactory;
    protected $table = 'view_aset';
    protected $guarded = ['id'];
    public $timestamps = false;
    
}
