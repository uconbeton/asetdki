<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewModul extends Model
{
    use HasFactory;
    protected $table = 'view_modul';
    protected $guarded = ['id'];
    public $timestamps = false;
    
}
