<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewDashboard extends Model
{
    use HasFactory;
    protected $table = 'view_dashboard';
    protected $guarded = ['id'];
    public $timestamps = false;
    
}
