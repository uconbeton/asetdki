<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ObatController;
use App\Http\Controllers\PasienController;
use App\Http\Controllers\PoliController;
use App\Http\Controllers\DokterController;
use App\Http\Controllers\TransaksiobatController;
use App\Http\Controllers\AsuransiController;
use App\Http\Controllers\Master\BidangController;
use App\Http\Controllers\Master\KategoriasetController;
use App\Http\Controllers\Master\SatuanasetController;
use App\Http\Controllers\Setting\RoleController;
use App\Http\Controllers\Setting\AksesController;
use App\Http\Controllers\Setting\PenggunaController;
use App\Http\Controllers\Master\SumberasetController;
use App\Http\Controllers\Transaksi\PeminjamanController;
use App\Http\Controllers\Transaksi\PersediaanController;
use App\Http\Controllers\Transaksi\AsetController;
use App\Http\Controllers\Master\JenisasetController;
use App\Http\Controllers\Master\KategoriController;
use App\Http\Controllers\Master\BahanController;
use App\Http\Controllers\RawatJalanController;
use App\Http\Controllers\DiagnosaController;
use App\Http\Controllers\Auth\LogoutController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/forget-password', [LogoutController::class, 'forget_password']);
Route::post('/store-reset', [LogoutController::class, 'store_reset']);
Route::group(['middleware' => 'auth'], function() {
    /**
    * Logout Route
    */
    Route::get('/logout-perform', [LogoutController::class, 'perform'])->name('logout.perform');
    
 });

Route::group(['middleware' => 'auth','prefix' => 'setting'],function(){
    Route::get('/', [RoleController::class, 'index']);
    Route::group(['prefix' => 'role'],function(){
        Route::get('/', [RoleController::class, 'index']);
        Route::get('/view', [RoleController::class, 'view']);
        Route::post('/', [RoleController::class, 'store']);
        Route::post('/store_akses', [RoleController::class, 'store_akses']);
        Route::get('/modal', [RoleController::class, 'modal']);
        Route::get('/switch_status', [RoleController::class, 'switch_status']);
        Route::get('/delete', [RoleController::class, 'delete_data']);
        Route::get('/getdata', [RoleController::class, 'get_data']);
        Route::get('/getdatadetail', [RoleController::class, 'get_data_detail']);
    });
    Route::group(['prefix' => 'akses'],function(){
        Route::get('/', [AksesController::class, 'index']);
        Route::get('/view', [AksesController::class, 'view']);
        Route::post('/', [AksesController::class, 'store']);
        Route::post('/store_akses', [AksesController::class, 'store_akses']);
        Route::get('/modal', [AksesController::class, 'modal']);
        Route::get('/switch_status', [AksesController::class, 'switch_status']);
        Route::get('/delete', [AksesController::class, 'delete_data']);
        Route::get('/getdata', [AksesController::class, 'get_data']);
        Route::get('/getdatadetail', [AksesController::class, 'get_data_detail']);
    });
    Route::group(['prefix' => 'pengguna'],function(){
        Route::get('/', [PenggunaController::class, 'index']);
        Route::get('/view', [PenggunaController::class, 'view']);
        Route::post('/', [PenggunaController::class, 'store']);
        Route::post('/store_Pengguna', [PenggunaController::class, 'store_Pengguna']);
        Route::get('/modal', [PenggunaController::class, 'modal']);
        Route::get('/switch_status', [PenggunaController::class, 'switch_status']);
        Route::get('/delete', [PenggunaController::class, 'delete_data']);
        Route::get('/getdata', [PenggunaController::class, 'get_data']);
        Route::get('/getdatadetail', [PenggunaController::class, 'get_data_detail']);
    });
    
});
Route::group(['middleware' => 'auth','prefix' => 'transaksi-tetap'],function(){
    Route::get('/', [PeminjamanController::class, 'index']);
    Route::group(['prefix' => 'peminjaman-aset'],function(){
        Route::get('/', [PeminjamanController::class, 'index']);
        Route::get('/view', [PeminjamanController::class, 'view']);
        Route::post('/', [PeminjamanController::class, 'store']);
        Route::post('/store_peminjaman', [PeminjamanController::class, 'store_peminjaman']);
        Route::get('/modal', [PeminjamanController::class, 'modal']);
        Route::get('/switch_status', [PeminjamanController::class, 'switch_status']);
        Route::get('/delete', [PeminjamanController::class, 'delete_data']);
        Route::get('/getdata', [PeminjamanController::class, 'get_data']);
        Route::get('/getdatadetail', [PeminjamanController::class, 'get_data_detail']);
        Route::get('/getdatadetail', [PeminjamanController::class, 'get_data_detail']);
    });
    Route::group(['prefix' => 'persediaan-aset'],function(){
        Route::get('/', [PersediaanController::class, 'index']);
        Route::get('/view', [PersediaanController::class, 'view']);
        Route::post('/', [PersediaanController::class, 'store']);
        Route::post('/store_pengajuan', [PersediaanController::class, 'store_pengajuan']);
        Route::get('/modal', [PersediaanController::class, 'modal']);
        Route::get('/switch_status', [PersediaanController::class, 'switch_status']);
        Route::get('/delete', [PersediaanController::class, 'delete_data']);
        Route::get('/getdata', [PersediaanController::class, 'get_data']);
        Route::get('/getdatadetail', [PersediaanController::class, 'get_data_detail']);
    });
});
Route::group(['middleware' => 'auth','prefix' => 'transaksi-lancar'],function(){
    Route::get('/', [PersediaanController::class, 'index_lancar']);
    
    Route::group(['prefix' => 'persediaan-aset'],function(){
        Route::get('/', [PersediaanController::class, 'index_lancar']);
        Route::get('/view', [PersediaanController::class, 'view_lancar']);
        Route::post('/', [PersediaanController::class, 'store_lancar']);
        Route::post('/store_pengajuan', [PersediaanController::class, 'store_pengajuan_lancar']);
        Route::get('/modal', [PersediaanController::class, 'modal_lancar']);
        Route::get('/switch_status', [PersediaanController::class, 'switch_status_lancar']);
        Route::get('/delete', [PersediaanController::class, 'delete_data_lancar']);
        Route::get('/getdata', [PersediaanController::class, 'get_data_lancar']);
        Route::get('/getdatadetail', [PersediaanController::class, 'get_data_detail_lancar']);
    });
});
Route::group(['middleware' => 'auth','prefix' => 'aset'],function(){
    Route::get('/', [AsetController::class, 'index']);
    Route::get('/getapi', [AsetController::class, 'get_api_aset']);
    Route::get('/getdataall', [AsetController::class, 'get_data_aset']);
    Route::group(['prefix' => 'daftar-aset'],function(){
        Route::get('/', [AsetController::class, 'index']);
        Route::get('/view', [AsetController::class, 'view']);
        Route::post('/', [AsetController::class, 'store']);
        Route::get('/modal', [AsetController::class, 'modal']);
        Route::get('/modal_detail', [AsetController::class, 'modal_detail']);
        Route::get('/switch_status', [AsetController::class, 'switch_status']);
        Route::get('/delete', [AsetController::class, 'delete_data']);
        Route::get('/getdata', [AsetController::class, 'get_data']);
    });
    Route::group(['prefix' => 'daftar-aset-non'],function(){
        Route::get('/', [AsetController::class, 'index_non']);
        Route::get('/view', [AsetController::class, 'view_non']);
        Route::post('/', [AsetController::class, 'store_non']);
        Route::get('/modal', [AsetController::class, 'modal_non']);
        Route::get('/switch_status', [AsetController::class, 'switch_status_non']);
        Route::get('/delete', [AsetController::class, 'delete_data_non']);
        Route::get('/getdata', [AsetController::class, 'get_data_non']);
    });
});
Route::group(['middleware' => 'auth','prefix' => 'master'],function(){
    Route::group(['prefix' => 'bidang'],function(){
        Route::get('/', [BidangController::class, 'index']);
        Route::get('/view', [BidangController::class, 'view']);
        Route::post('/', [BidangController::class, 'store']);
        Route::get('/modal', [BidangController::class, 'modal']);
        Route::get('/switch_status', [BidangController::class, 'switch_status']);
        Route::get('/delete', [BidangController::class, 'delete_data']);
        Route::get('/getdata', [BidangController::class, 'get_data']);
    });
    Route::group(['prefix' => 'kategori-aset'],function(){
        Route::get('/', [KategoriController::class, 'index']);
        Route::get('/view', [KategoriController::class, 'view']);
        Route::post('/', [KategoriController::class, 'store']);
        Route::get('/modal', [KategoriController::class, 'modal']);
        Route::get('/switch_status', [KategoriController::class, 'switch_status']);
        Route::get('/delete', [KategoriController::class, 'delete_data']);
        Route::get('/getdata', [KategoriController::class, 'get_data']);
    });
    Route::group(['prefix' => 'bahan-aset'],function(){
        Route::get('/', [BahanController::class, 'index']);
        Route::get('/view', [BahanController::class, 'view']);
        Route::post('/', [BahanController::class, 'store']);
        Route::get('/modal', [BahanController::class, 'modal']);
        Route::get('/switch_status', [BahanController::class, 'switch_status']);
        Route::get('/delete', [BahanController::class, 'delete_data']);
        Route::get('/getdata', [BahanController::class, 'get_data']);
    });
    Route::group(['prefix' => 'jenis-aset'],function(){
        Route::get('/', [JenisasetController::class, 'index']);
        Route::get('/view', [JenisasetController::class, 'view']);
        Route::post('/', [JenisasetController::class, 'store']);
        Route::get('/modal', [JenisasetController::class, 'modal']);
        Route::get('/switch_status', [JenisasetController::class, 'switch_status']);
        Route::get('/delete', [JenisasetController::class, 'delete_data']);
        Route::get('/getdata', [JenisasetController::class, 'get_data']);
    });
    Route::group(['prefix' => 'satuan-aset'],function(){
        Route::get('/', [SatuanasetController::class, 'index']);
        Route::get('/view', [SatuanasetController::class, 'view']);
        Route::post('/', [SatuanasetController::class, 'store']);
        Route::get('/modal', [SatuanasetController::class, 'modal']);
        Route::get('/switch_status', [SatuanasetController::class, 'switch_status']);
        Route::get('/delete', [SatuanasetController::class, 'delete_data']);
        Route::get('/getdata', [SatuanasetController::class, 'get_data']);
    });
    Route::group(['prefix' => 'sumber-aset'],function(){
        Route::get('/', [SumberasetController::class, 'index']);
        Route::get('/view', [SumberasetController::class, 'view']);
        Route::post('/', [SumberasetController::class, 'store']);
        Route::get('/modal', [SumberasetController::class, 'modal']);
        Route::get('/switch_status', [SumberasetController::class, 'switch_status']);
        Route::get('/delete', [SumberasetController::class, 'delete_data']);
        Route::get('/getdata', [SumberasetController::class, 'get_data']);
    });
    Route::group(['prefix' => 'bidang'],function(){
        Route::get('/', [BidangController::class, 'index']);
        Route::get('/view', [BidangController::class, 'view']);
        Route::post('/', [BidangController::class, 'store']);
        Route::get('/modal', [BidangController::class, 'modal']);
        Route::get('/switch_status', [BidangController::class, 'switch_status']);
        Route::get('/delete', [BidangController::class, 'delete_data']);
        Route::get('/getdata', [BidangController::class, 'get_data']);
    });
});
Route::group(['middleware' => 'auth','prefix' => 'user'],function(){
    Route::get('/', [UserController::class, 'index']);
    Route::get('/view', [UserController::class, 'view']);
    Route::post('/', [UserController::class, 'store']);
    Route::get('/modal', [UserController::class, 'modal']);
    Route::get('/switch_status', [UserController::class, 'switch_status']);
    Route::get('/delete', [UserController::class, 'delete_data']);
    Route::get('/getdata', [UserController::class, 'get_data']);
});



Auth::routes();

Route::get('/aset-pusdatin/{id?}', [AsetController::class, 'cek_aset']);
Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home/getdatadashboard', [App\Http\Controllers\HomeController::class, 'get_data_dashboard']);
Route::get('/home/getdatadashboardpengguna', [App\Http\Controllers\HomeController::class, 'get_data_dashboard_pengguna']);
