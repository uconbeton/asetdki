            	<ul class="nav">
					<li class="nav-header">Navigation</li>
                    <li>
						<a href="{{url('/')}}">
							<i class="fa fa-th-large"></i>
							<span>Home </span> 
						</a>
					</li>
					
					
						
						

						@if(judul_modul(1)!=0 || judul_modul(2)!=0)
							<li class="nav-header">Master</li>
							
							<li class="has-sub @if((Request::is('master')==1 || Request::is('master/*')==1)  ) active  @endif">
								<a href="javascript:;">
									<b class="caret"></b>
									<i class="fa fa-sitemap"></i>
									<span>Master Data </span>
								</a>
								<ul class="sub-menu">
									@foreach(get_akses_modul(1) as $mod)
										<li><a href="{{url($mod->link)}}"><i class="fas fa-caret-right"></i> {{$mod->modul}}</a></li>
									@endforeach
									
								</ul>
							</li>
							@if(judul_modul(2)!=0)
								<li class="has-sub @if((Request::is('aset')==1 || Request::is('aset/*')==1)  ) active  @endif">
									<a href="javascript:;">
										<b class="caret"></b>
										<i class="fa fa-archive"></i>
										<span>Aset </span>
									</a>
									<ul class="sub-menu">
										<li class="has-sub @if((Request::is('aset/daftar-aset')==1 || Request::is('aset/daftar-aset/*')==1)  ) active  @endif">
											<a href="javascript:;">
												<b class="caret"></b>
												<i class="fas fa-caret-right"></i> Aset Tetap
											</a>
											<ul class="sub-menu">
												@foreach(get_kategori_aset(1) as $mod)
													<li><a href="{{url('aset/daftar-aset')}}?tipe={{encoder($mod->id)}}"><i class="fas fa-caret-right"></i> {{$mod->kode_kategori}} ({{$mod->kategori}})</a></li>
												@endforeach
											</ul>
										</li>
										<li class="has-sub @if((Request::is('aset/daftar-aset')==1 || Request::is('aset/daftar-aset/*')==1)  ) active  @endif">
											<a href="javascript:;">
												<b class="caret"></b>
												<i class="fas fa-caret-right"></i> Aset Lancar
											</a>
											<ul class="sub-menu">
												@foreach(get_kategori_aset(2) as $mod)
													<li><a href="{{url('aset/daftar-aset')}}?tipe={{encoder($mod->id)}}"><i class="fas fa-caret-right"></i> {{$mod->kode_kategori}}</a></li>
												@endforeach
											</ul>
										</li>
										
									</ul>
								</li>
								
								

							@endif

						@endif
						
						@if(judul_modul(3)!=0 || judul_modul(4)!=0)
							<li class="nav-header">Transaksi</li>
							@if(akses_modul(8)>0 || akses_modul(9)>0)
							<li class="has-sub @if((Request::is('transaksi-tetap/')==1 || Request::is('transaksi-tetap/*')==1)   ) active  @endif">
								<a href="javascript:;">
									<b class="caret"></b>
									<i class="fa fa-cubes"></i>
									<span>Aset Tetap </span>
								</a>
								<ul class="sub-menu">
									@foreach(get_akses_modul(3) as $mod)
										<li><a href="{{url($mod->link)}}"><i class="fas fa-caret-right"></i> {{$mod->modul}}</a></li>
									@endforeach
									
								</ul>
							</li>
							@endif
							@if(akses_modul(10)>0)
							<li class="has-sub @if((Request::is('transaksi-lancar/')==1 || Request::is('transaksi-lancar/*')==1)   ) active  @endif">
								<a href="javascript:;">
									<b class="caret"></b>
									<i class="fa fa-cubes"></i>
									<span>Aset Lancar </span>
								</a>
								<ul class="sub-menu">
									@foreach(get_akses_modul(4) as $mod)
										<li><a href="{{url($mod->link)}}"><i class="fas fa-caret-right"></i> {{$mod->modul}}</a></li>
									@endforeach
									
								</ul>
							</li>
							@endif

						@endif
							
						
						
						@if(Auth::user()->role_id==1)
							<li class="nav-header">Setting</li>
							<li class="@if((Request::is('setting/pengguna')==1 || Request::is('setting/pengguna/*')==1)  ) active  @endif">
								<a href="{{url('/setting/pengguna')}}">
									<i class="fa fa-user-circle"></i>
									<span>Kelola Pengguna </span> 
								</a>
							</li>
							<li class="@if((Request::is('setting/role')==1 || Request::is('setting/role/*')==1)  ) active  @endif">
								<a href="{{url('/setting/role')}}">
									<i class="fa fa-users"></i>
									<span>Role Otorisasi </span> 
								</a>
							</li>
							<li class="@if((Request::is('setting/akses')==1 || Request::is('setting/akses/*')==1)  ) active  @endif">
								<a href="{{url('/setting/akses')}}">
									<i class="fa fa-lock"></i>
									<span>Perizinan Akses </span> 
								</a>
							</li>
							
							
						@endif
				</ul>