
@extends('layouts.app')
@push('datatable')
    <link href="{{url_plug()}}/assets/plugins/switchery/switchery.min.css" rel="stylesheet" />
	<link href="{{url_plug()}}/assets/plugins/abpetkov-powerange/dist/powerange.min.css" rel="stylesheet" />
    <script src="{{url_plug()}}/assets/plugins/switchery/switchery.min.js"></script>
	<script src="{{url_plug()}}/assets/plugins/abpetkov-powerange/dist/powerange.min.js"></script>
    <script src="{{url_plug()}}/assets/js/demo/form-slider-switcher.demo.js"></script>
    <script type="text/javascript">
        /*
        Template Name: Color Admin - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
        Version: 4.6.0
        Author: Sean Ngu
        Website: http://www.seantheme.com/color-admin/admin/
        */
        
        function show_data() {
            if ($('#data-table-fixed-header').length !== 0) {
                var table=$('#data-table-fixed-header').DataTable({
                    lengthMenu: [20, 40, 60],
                    lengthChange:false,
                    fixedHeader: {
                        header: true,
                        headerOffset: $('#header').height()
                    },
                    responsive: false,
                    ajax:"{{ url('master/kategori/getdata')}}",
                    dom: 'lrtip',
					columns: [
                        { data: 'id', render: function (data, type, row, meta) 
							{
								return meta.row + meta.settings._iDisplayStart + 1;
							} 
						},
						{ data: 'action' },
						{ data: 'kode_kategori' },
						{ data: 'kategori' },
						
					],
					language: {
						paginate: {
							// remove previous & next text from pagination
							previous: '<< previous',
							next: 'Next>>'
						}
					}
                });
                $('#cari_data').keyup(function(){
                    table.search($(this).val()).draw() ;
                })
            }
        };


        $(document).ready(function() {
			show_data();

		});

		
    </script>
@endpush
@section('content')
    

        <div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb float-xl-right">
				<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
				<li class="breadcrumb-item active">Bidang</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Daftar Bidang <small></small></h1>
			<!-- end page-header -->
			<!-- begin row -->
			<div class="row">
				
				<div class="col-xl-12 ui-sortable">
					<!-- begin panel -->
					<div class="panel panel-inverse">
						<!-- begin panel-heading -->
						<div class="panel-heading ui-sortable-handle">
							<h4 class="panel-title">&nbsp;</h4>
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						<!-- end panel-heading -->
						<!-- begin alert -->
						
						<div class="panel-body">
							<div class="row" style="margin-bottom:2%">
                                <div class="col-md-8">
                                    <a href="javascript:;" onclick="tambah(`{{encoder(0)}}`)" class="btn btn-primary m-r-5"><i class="fa fa-plus"></i> Tambah Baru</a>
                                </div>
                                <div class="col-md-4">
                                    <input class="form-control" id="cari_data" placeholder="Cari......" type="text" />
                                </div>
                            </div>
                            <table class="table table-striped table-bordered table-td-valign-middle dataTable no-footer" id="data-table-fixed-header"  >
                                <thead>
                                    <tr role="row">
                                        <th width="5%">No</th>
                                        <th width="5%"></th>
                                        <th width="10%">Kode</th>
                                        <th width="">Kategori</th>
                                    </tr>
                                </thead>
                            </table>
								
						</div>
						<!-- end panel-body -->
					</div>
					<!-- end panel -->
				</div>
				<!-- end col-10 -->
			</div>
			<!-- end row -->
		</div> 
        <div class="modal fade" id="modal-form" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Form Bidang</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form  id="mydata" method="post" action="{{ url('master/kategori') }}" enctype="multipart/form-data" >
                            @csrf
                            <div id="tampil-form"></div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:;" class="btn btn-danger" data-dismiss="modal">Batal</a>
                        <a href="javascript:;" class="btn btn-primary" onclick="simpan_data()"><i class="fas fa-save"></i> Proses</a>
                    </div>
                </div>
            </div>
        </div>      
@endsection
@push('ajax')
        
        <script type="text/javascript">
			
			function tambah(id){
                $('#modal-form').modal('show');
                $('#tampil-form').load("{{url('master/kategori/modal')}}?id="+id);
			
			} 
            function hanyaAngka(evt) {
				
				var charCode = (evt.which) ? evt.which : event.keyCode
				if ((charCode > 47 && charCode < 58 ) || (charCode > 96 && charCode < 123 ) || charCode==95 ){
					
					return true;
				}else{
					return false;
				}
		    }
            function simpan_data(){
            
                var form=document.getElementById('mydata');
                    $.ajax({
                        type: 'POST',
                        url: "{{ url('master/kategori') }}",
                        data: new FormData(form),
                        contentType: false,
                        cache: false,
                        processData:false,
                        beforeSend: function() {
                            document.getElementById("loadnya").style.width = "100%";
                        },
                        success: function(msg){
                            var bat=msg.split('@');
                            if(bat[1]=='ok'){
                                document.getElementById("loadnya").style.width = "0px";
                                swal({
                                    title: 'Sukses diproses',
                                    text: '',
                                    icon: 'success',
                                    buttons: {
                                        cancel: {
                                            text: 'Tutup',
                                            value: null,
                                            visible: true,
                                            className: 'btn btn-default',
                                            closeModal: true,
                                        },
                                        
                                    }
                                });
                                $('#modal-form').modal('hide');
                                $('#tampil-form').html("");
                                var tables=$('#data-table-fixed-header').DataTable();
                                    tables.ajax.url("{{ url('master/kategori/getdata')}}").load();
                            }else{
                                document.getElementById("loadnya").style.width = "0px";
                                
                                swal({
                                    title: 'Opps Error!',
                                    html:true,
                                    text:'ss',
                                    icon: 'error',
                                    buttons: {
                                        cancel: {
                                            text: 'Tutup',
                                            value: null,
                                            visible: true,
                                            className: 'btn btn-default',
                                            closeModal: true,
                                        },
                                        
                                    }
                                });
                                $('.swal-text').html('<div style="width:100%;background:#f2f2f5;padding:2%;text-align:left;font-size:13px">'+msg+'</div>')
                            }
                            
                            
                        }
                    });
            }
            function delete_data(id){
                    swal({
                            title: "Yakin menghapus data ini ?",
                            text: "data akan hilang dari daftar  ini",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                        })
                        .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                type: 'GET',
                                url: "{{url('master/kategori/delete')}}",
                                data: "id="+id,
                                success: function(msg){
                                    swal("Sukses diproses", "", "success")
                                    var tables=$('#data-table-fixed-header').DataTable();
                                        tables.ajax.url("{{ url('master/kategori/getdata')}}").load();
                                }
                            });
                           
                        } else {
                            var tables=$('#data-table-fixed-header').DataTable();
                                tables.ajax.url("{{ url('master/kategori/getdata')}}").load();
                        }
                    });
                    
                
            } 
            function switch_data(id,act){
                if(act==1){
                    

                    swal({
                            title: "Aktifkan Data Ini?",
                            text: "",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                        })
                        .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                type: 'GET',
                                url: "{{url('master/kategori/switch_status')}}",
                                data: "id="+id+"&act="+act,
                                success: function(msg){
                                    swal("Sukses diproses", "", "success")
                                    var tables=$('#data-table-fixed-header').DataTable();
                                        tables.ajax.url("{{ url('master/kategori/getdata')}}").load();
                                }
                            });
                           
                        } else {
                            var tables=$('#data-table-fixed-header').DataTable();
                                tables.ajax.url("{{ url('master/kategori/getdata')}}").load();
                        }
                    });
                }
                if(act==0){
                    swal({
                            title: "Non aktifkan  Data Ini?",
                            text: "",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                        })
                        .then((willDelete) => {
                        if (willDelete) {
                            $.ajax({
                                type: 'GET',
                                url: "{{url('master/kategori/switch_status')}}",
                                data: "id="+id+"&act="+act,
                                success: function(msg){
                                    swal("Sukses diproses", "", "success")
                                    var tables=$('#data-table-fixed-header').DataTable();
                                        tables.ajax.url("{{ url('master/kategori/getdata')}}").load();
                                }
                            });
                           
                        } else {
                            var tables=$('#data-table-fixed-header').DataTable();
                                tables.ajax.url("{{ url('master/kategori/getdata')}}").load();
                        }
                    });
                }
                
            }
        </script>
@endpush