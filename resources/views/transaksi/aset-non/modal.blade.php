<input type="hidden" name="id" value="{{$id}}">

<div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">Nama Aset</label>
	</div>
	<div class="col-lg-8">
		<input type="text" class="form-control form-control-sm"  name="nama_aset" value="{{$data->nama_aset}}" placeholder="Enter.....">
	</div>
	
</div>
<div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">No Serial Aset</label>
	</div>
	<div class="col-lg-5">
		<input type="text" class="form-control form-control-sm"  name="serial_number" value="{{$data->serial_number}}" placeholder="Enter.....">
	</div>
	
</div>
<div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">Kategori  Aset</label>
	</div>
	<div class="col-lg-5">
		<select class="form-control form-control-sm" name="kategori_aset_id">
			<option value="">::. Pilih - -</option>
			@foreach(get_kategori_aset(2) as $gt)
				<option value="{{$gt->id}}" @if($data->kategori_aset_id==$gt->id) selected @endif >[{{$gt->kode_kategori}}]  {{$gt->kategori}}</option>
			@endforeach
		</select>
	</div>
	
	
</div>
<div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">Asal Oleh / Sumber  Aset</label>
	</div>
	<div class="col-lg-7">
		<select class="form-control form-control-sm" name="sumber_aset_id">
			<option value="">::. Pilih - -</option>
			@foreach(get_sumber_aset() as $gt)
				<option value="{{$gt->id}}" @if($data->sumber_aset_id==$gt->id) selected @endif >-  {{$gt->sumber_aset}}</option>
			@endforeach
		</select>
	</div>
	
	
</div>
<div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">Satuan & Jenis  Aset</label>
	</div>
	<div class="col-lg-3">
		<select class="form-control form-control-sm" name="satuan_id">
			<option value="">::. Pilih - -</option>
			@foreach(get_satuan_aset() as $gt)
				<option value="{{$gt->id}}" @if($data->satuan_id==$gt->id) selected @endif >  {{$gt->satuan_aset}}</option>
			@endforeach
		</select>
	</div>
	<div class="col-lg-4">
		<select class="form-control form-control-sm" name="jenis_aset_id">
			<option value="">::. Pilih - -</option>
			@foreach(get_jenis_aset() as $gt)
				<option value="{{$gt->id}}" @if($data->jenis_aset_id==$gt->id) selected @endif > {{$gt->jenis_aset}}</option>
			@endforeach
		</select>
	</div>
	
</div>

<div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">Spesifikasi</label>
	</div>
	<div class="col-lg-8">
		<textarea rows="3" class="form-control form-control-sm"  name="spesifikasi" " placeholder="Enter.....">{{$data->spesifikasi}}</textarea>
	</div>
	
</div>
<!-- <div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">User Role</label>
	</div>
	<div class="col-lg-6">
		<select class="form-control form-control-sm" name="role_id" >
			<option value="">Select --</option>
			<option value="1">Admin</option>
			<option value="2">User Monitoring</option>
			
		</select>
		
	</div>
	
</div>
<div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">Password</label>
	</div>
	<div class="col-lg-8">
		<input type="password" class="form-control form-control-sm"  name="password" value="{{$data->password}}" placeholder="Enter.....">
	</div>
	
</div> -->


<script>
	function hanyaAngka(evt) {
				
				var charCode = (evt.which) ? evt.which : event.keyCode
				if ((charCode > 47 && charCode < 58 ) || (charCode > 96 && charCode < 123 ) || charCode==95 ){
					
					return true;
				}else{
					return false;
				}
		
				// 	return false;
				// return true;
				// alert(charCode)
			}
</script>
