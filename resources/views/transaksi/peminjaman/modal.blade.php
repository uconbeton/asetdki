<input type="hidden" name="peminjaman_id" value="{{$peminjaman_id}}">
<input type="hidden" name="aset_id" value="{{$id}}">

<div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">Nama Aset</label>
	</div>
	<div class="col-lg-8">
		<input type="text" disabled class="form-control form-control-sm"  name="nama_aset" value="{{$data->nama_aset}}" placeholder="Enter.....">
	</div>
	
</div>
<div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">No Serial Aset</label>
	</div>
	<div class="col-lg-5">
		<input type="text" disabled class="form-control form-control-sm"  name="serial_number" value="{{$data->serial_number}}" placeholder="Enter.....">
	</div>
	
</div>
<div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">Kategori  Aset</label>
	</div>
	<div class="col-lg-5">
		<input type="text" disabled class="form-control form-control-sm"   value="{{$data->kategori}}" placeholder="Enter.....">
	</div>
	
</div>

<div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">Asal Oleh / Sumber  Aset</label>
	</div>
	<div class="col-lg-7">
		<input type="text" disabled class="form-control form-control-sm"   value="{{$data->sumber_aset}}" placeholder="Enter.....">
		
	</div>
	
	
</div>
<div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">Satuan & Jenis  Aset</label>
	</div>
	<div class="col-lg-3">
		<input type="text" disabled class="form-control form-control-sm"   value="{{$data->satuan_aset}}" placeholder="Enter.....">
	</div>
	<div class="col-lg-4">
		<input type="text" disabled class="form-control form-control-sm"   value="{{$data->jenis_aset}}" placeholder="Enter.....">
	</div>
	
</div>

<div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">Spesifikasi</label>
	</div>
	<div class="col-lg-8">
		<textarea rows="3" class="form-control form-control-sm" disabled name="spesifikasi" " placeholder="Enter.....">{{$data->spesifikasi}}</textarea>
	</div>
	
</div>
<div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">Jumlah Permintaan</label>
	</div>
	<div class="col-lg-3">
		<input type="number"  class="form-control form-control-sm"  name="qty" value="{{$qty}}" placeholder="Enter.....">
	</div>
	
</div>
<!-- <div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">User Role</label>
	</div>
	<div class="col-lg-6">
		<select class="form-control form-control-sm" name="role_id" >
			<option value="">Select --</option>
			<option value="1">Admin</option>
			<option value="2">User Monitoring</option>
			
		</select>
		
	</div>
	
</div>
<div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">Password</label>
	</div>
	<div class="col-lg-8">
		<input type="password" class="form-control form-control-sm"  name="password" value="{{$data->password}}" placeholder="Enter.....">
	</div>
	
</div> -->


<script>
	function hanyaAngka(evt) {
				
				var charCode = (evt.which) ? evt.which : event.keyCode
				if ((charCode > 47 && charCode < 58 ) || (charCode > 96 && charCode < 123 ) || charCode==95 ){
					
					return true;
				}else{
					return false;
				}
		
				// 	return false;
				// return true;
				// alert(charCode)
			}
</script>
