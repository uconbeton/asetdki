
@extends('layouts.app')
@push('datatable')
 
    <script type="text/javascript">
        /*
        Template Name: Color Admin - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
        Version: 4.6.0
        Author: Sean Ngu
        Website: http://www.seantheme.com/color-admin/admin/
        */
        
        

        function show_data() {
            if ($('#data-table-fixed-header-aset').length !== 0) {
                var table=$('#data-table-fixed-header-aset').DataTable({
                    lengthMenu: [50, 100, 500],
                    lengthChange:false,
                    fixedHeader: {
                        header: true,
                        headerOffset: $('#header').height()
                    },
                    responsive: true,
                    ajax:"{{ url('aset/getdataall')}}",
                    columns: [
                        { data: 'id', render: function (data, type, row, meta) 
							{
								return meta.row + meta.settings._iDisplayStart + 1;
							} 
						},
                        { data: 'action' },
						{ data: 'nama_aset' },
						{ data: 'kategori' },
						{ data: 'jenis_aset' },
						{ data: 'satuan_aset' },
						{ data: 'serial_number' },
						{ data: 'sumber_aset' },
						
					],
					language: {
						paginate: {
							// remove previous & next text from pagination
							previous: '<< previous',
							next: 'Next>>'
						}
					}
                });
                $('#cari_data').keyup(function(){
                    table.search($(this).val()).draw() ;
                })
            }
        };
        function show_data_detail() {
            if ($('#data-table-fixed-header').length !== 0) {
                var table=$('#data-table-fixed-header').DataTable({
                    lengthMenu: [50, 100, 500],
                    lengthChange:false,
                    fixedHeader: {
                        header: true,
                        headerOffset: $('#header').height()
                    },
                    responsive: false,
                    ajax:"{{ url('transaksi-tetap/peminjaman-aset/getdatadetail')}}?peminjaman_id={{$id}}",
                    columns: [
                        { data: 'id', render: function (data, type, row, meta) 
							{
								return meta.row + meta.settings._iDisplayStart + 1;
							} 
						},
                        { data: 'action' },
						{ data: 'nama_aset' },
						{ data: 'satuan_aset' },
						{ data: 'qty' },
						
					],
					language: {
						paginate: {
							// remove previous & next text from pagination
							previous: '<< previous',
							next: 'Next>>'
						}
					}
                });
                $('#cari_data_detail').keyup(function(){
                    table.search($(this).val()).draw() ;
                })
            }
        };


        $(document).ready(function() {
			
			show_data_detail();

		});

		
    </script>
@endpush
@section('content')
    

        <div id="content" class="content">
			<ol class="breadcrumb float-xl-right">
				<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
				<li class="breadcrumb-item active">Peminjaman Aset</li>
			</ol>
			<h1 class="page-header">Formulir Peminjaman Aset<small></small></h1>
			<div class="row">
				
				<div class="col-xl-12 ui-sortable">
					<!-- begin panel -->
					<div class="panel panel-inverse">
						<!-- begin panel-heading -->
						<div class="panel-heading ui-sortable-handle">
							<h4 class="panel-title">&nbsp;</h4>
							<div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						<!-- end panel-heading -->
						<!-- begin alert -->
						
						<div class="panel-body">
                            <form  id="mydata" method="post" action="{{ url('user') }}" enctype="multipart/form-data" >
                                 @csrf
                                <div class="row">
                                    <input type="hidden" name="id" value="{{$id}}">
                                    <div class="col-xl-12 ">
                                        <legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse">Formulir Pengisian Peminjaman Aset</legend>
                                    </div>
                                    <div class="col-xl-12 ">
                                        @if($id>0)
                                        <div class="form-group row m-b-10">
                                            <label class="col-lg-3 text-lg-right col-form-label">No Pengajuan <span class="text-danger">*</span></label>
                                            <div class="col-lg-9 col-xl-2">
                                                <input type="text" disabled value="{{$data->nomor}}" placeholder="Ketik...." class="form-control">
                                            </div>
                                        </div>
                                        @endif
                                        <div class="form-group row m-b-2">
                                            <label class="col-lg-3 text-lg-right col-form-label label-col">ID Peminjam (PIC) </label>
                                            <div class="col-lg-9 col-xl-2">
                                                <input type="text" name="nrk" value="{{$data->nrk}}" placeholder="NRK" class="form-control form-control-sm">
                                            </div>
                                            <div class="col-lg-9 col-xl-7">
                                                <input type="text" name="nama_peminjam" value="{{$data->nama_peminjam}}" placeholder="Nama Peminjam" class="form-control form-control-sm">
                                            </div>
                                        </div>
                                        <div class="form-group row m-b-2">
                                            <label class="col-lg-3 text-lg-right col-form-label label-col">Bidang </label>
                                            <div class="col-lg-9 col-xl-6">
                                            
                                                <select class="form-control form-control-sm" onchange="pilih_bidang(this.value)" name="bidang_id">
                                                    <option value="">-- Pilih --</option>
                                                    @foreach(get_bidang() as $gt)
                                                        <option value="{{$gt->id}}" @if($data->bidang_id==$gt->id) selected @endif >- {{$gt->bidang}}</option>
                                                    @endforeach
                                                </select>
                                                    
                                            </div>
                                        </div>
                                        <div class="form-group row m-b-2" id="lainnya">
                                            <label class="col-lg-3 text-lg-right col-form-label label-col">Keterangan Peminjam </label>
                                            <div class="col-lg-9 col-xl-9">
                                                <input type="text" name="keterangan_peminjam" value="{{$data->keterangan_peminjam}}" placeholder="Nama Peminjam" class="form-control form-control-sm">
                                            </div>
                                        </div>
                                        <div class="form-group row m-b-2">
                                            <label class="col-lg-3 text-lg-right col-form-label label-col">Tgl Peminjaman & Pengembalian</label>
                                            <div class="col-lg-9 col-xl-2">
                                            
                                                <div class="input-group date" id="datetimepicker1">
                                                    <input type="text" name="tanggal" value="{{$data->tanggal}}" class="form-control form-control-sm" />
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                </div>
                                                    
                                            </div>
                                            <div class="col-lg-9 col-xl-2">
                                            
                                                <div class="input-group date" id="datetimepicker2">
                                                    <input type="text" name="tanggal_sampai" value="{{$data->tanggal_sampai}}" class="form-control form-control-sm" />
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                </div>
                                                    
                                            </div>
                                        </div>
                                        <div class="form-group row m-b-2">
                                            <label class="col-lg-3 text-lg-right col-form-label label-col">Keterangan </label>
                                            <div class="col-lg-9 col-xl-7">
                                                <textarea rows="3" name="alasan_pinjam"  placeholder="Ketik...." class="form-control form-control-sm">{{$data->alasan_peminjaman}}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row m-b-2" id="lainnya">
                                            <label class="col-lg-3 text-lg-right col-form-label label-col">File Permintaan </label>
                                            <div class="col-lg-9 col-xl-4">
                                                <input type="file" name="surat" value="{{$data->surat}}" placeholder="Nama Peminjam" class="form-control form-control-sm">
                                            </div>
                                        </div>
                                        
                                        
                                    </div>
                                    
                                </div>
                            </form>	
						</div>
                        <div class="panel-body" style="margin-top:1%;background: #f5f5fb; padding: 1%;">
                            <div class="row">
                                <!-- begin col-8 -->
                                <div class="col-xl-12 ">
                                    <a href="javascript:;" onclick="kembali()" class="btn btn-sm btn-danger m-r-5"><i class="fa fa-step-backward"></i> Kembali</a>
                                    <a href="javascript:;"  onclick="add_item()"  class="btn btn-sm btn-success m-r-5"><i class="fa fa-plus"></i> Tambah Aset</a>
                                    <a href="javascript:;"  onclick="selesai()"  class="btn btn-sm btn-primary m-r-5"><i class="fa fa-save"></i> Selesai</a>
                                    
                                </div>
                                <div class="col-xl-12 ">
                                    <table class="table table-striped table-bordered table-td-valign-middle dataTable no-footer" id="data-table-fixed-header"  >
                                        <thead>
                                            <tr role="row">
                                                <th width="5%">No </th>
                                                <th width="5%"></th>
                                                <th >Nama Aset</th>
                                                <th width="10%">Satuan</th>
                                                <th width="9%">Qty</th>
                                            </tr>
                                        </thead>
                                    </table>
                                    
                                </div>
                            </div>
                        </div>
					</div>
				</div>
			</div>
		</div>  
        
        <div class="modal fade" id="modal-form-all" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Pilih aset yang akan diproses</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                            <table class="table table-striped table-bordered table-td-valign-middle dataTable no-footer" id="data-table-fixed-header-aset"  >
                                <thead>
                                    <tr role="row">
                                        <th width="5%">No</th>
                                        <th width="5%"></th>
                                        <th>Nama Aset</th>
                                        <th width="13%">Kategori</th>
                                        <th width="13%">Jenis</th>
                                        <th width="13%">Satuan</th>
                                        <th width="13%">No Serial</th>
                                        <th width="20%">Sumber</th>
                                    </tr>
                                </thead>
                            </table>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:;" class="btn btn-danger" data-dismiss="modal">Batal</a>
                    </div>
                </div>
            </div>
        </div>   
        <div class="modal fade" id="modal-form" aria-hidden="true" style="display: none;background: #202022a8;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Form Aset</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form  id="mydatadetail" method="post" action="{{ url('transaksi-tetap/peminjaman-aset') }}" enctype="multipart/form-data" >
                            @csrf
                            <div id="tampil-form"></div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:;" class="btn btn-danger" data-dismiss="modal">Batal</a>
                        <a href="javascript:;" class="btn btn-primary" onclick="simpan_data_detail()"><i class="fas fa-save"></i> Proses</a>
                    </div>
                </div>
            </div>
        </div>   
@endsection
@push('ajax')
        
        <script type="text/javascript">
			$('#lainnya').hide();
			function pilih_bidang(id){
                if(id==14){
                    $('#lainnya').show();
                }else{
                    $('#lainnya').hide();
                }
            }
			function add_item(id){
				@if($id>0)
                    $('#modal-form-all').modal('show');
                    show_data();
                @else
                    var form=document.getElementById('mydata');
                        $.ajax({
                            type: 'POST',
                            url: "{{ url('transaksi-tetap/peminjaman-aset') }}?act=0",
                            data: new FormData(form),
                            contentType: false,
                            cache: false,
                            processData:false,
                            beforeSend: function() {
                                document.getElementById("loadnya").style.width = "100%";
                            },
                            success: function(msg){
                                var bat=msg.split('@');
                                if(bat[1]=='ok'){
                                    document.getElementById("loadnya").style.width = "0px";
                                    
                                    location.assign("{{url('transaksi-tetap/peminjaman-aset/view')}}?id="+bat[2]+"&act="+bat[3])
                                }else{
                                    document.getElementById("loadnya").style.width = "0px";
                                    
                                    swal({
                                        title: 'Opps Error!',
                                        html:true,
                                        text:'ss',
                                        icon: 'error',
                                        buttons: {
                                            cancel: {
                                                text: 'Tutup',
                                                value: null,
                                                visible: true,
                                                className: 'btn btn-default',
                                                closeModal: true,
                                            },
                                            
                                        }
                                    });
                                    $('.swal-text').html('<div style="width:100%;background:#f2f2f5;padding:2%;text-align:left;font-size:13px">'+msg+'</div>')
                                }
                                
                                
                            }
                        });
                @endif
			} 
            @if($_GET['act']==1)
                $('#modal-form-all').modal('show');
                $('#tampil-form-all').load("{{url('transaksi-tetap/peminjaman-aset/modal')}}?peminjaman_id={{$id}}");
            @endif
			function pilih_aset(id,qty){
				$('#modal-form').modal('show');
                $('#tampil-form').load("{{url('transaksi-tetap/peminjaman-aset/modal')}}?id="+id+"&peminjaman_id={{$id}}&qty="+qty);
			} 
			function kembali(){
				location.assign("{{url('master/obat')}}")
			} 
            function hanyaAngka(evt) {
				
				var charCode = (evt.which) ? evt.which : event.keyCode
				if ((charCode > 47 && charCode < 58 ) || (charCode > 96 && charCode < 123 ) || charCode==95 ){
					
					return true;
				}else{
					return false;
				}
		    }
            function selesai(){
                var form=document.getElementById('mydata');
                        $.ajax({
                            type: 'POST',
                            url: "{{ url('transaksi-tetap/peminjaman-aset') }}?act=1",
                            data: new FormData(form),
                            contentType: false,
                            cache: false,
                            processData:false,
                            beforeSend: function() {
                                document.getElementById("loadnya").style.width = "100%";
                            },
                            success: function(msg){
                                var bat=msg.split('@');
                                if(bat[1]=='ok'){
                                    document.getElementById("loadnya").style.width = "0px";
                                    
                                    location.assign("{{url('transaksi-tetap/peminjaman-aset')}}")
                                }else{
                                    document.getElementById("loadnya").style.width = "0px";
                                    
                                    swal({
                                        title: 'Opps Error!',
                                        html:true,
                                        text:'ss',
                                        icon: 'error',
                                        buttons: {
                                            cancel: {
                                                text: 'Tutup',
                                                value: null,
                                                visible: true,
                                                className: 'btn btn-default',
                                                closeModal: true,
                                            },
                                            
                                        }
                                    });
                                    $('.swal-text').html('<div style="width:100%;background:#f2f2f5;padding:2%;text-align:left;font-size:13px">'+msg+'</div>')
                                }
                                
                                
                            }
                        });
            }
            function simpan_data_detail(){
            
                var form=document.getElementById('mydatadetail');
                    $.ajax({
                        type: 'POST',
                        url: "{{ url('transaksi-tetap/peminjaman-aset/store_peminjaman') }}",
                        data: new FormData(form),
                        contentType: false,
                        cache: false,
                        processData:false,
                        beforeSend: function() {
                            document.getElementById("loadnya").style.width = "100%";
                        },
                        success: function(msg){
                            var bat=msg.split('@');
                            if(bat[1]=='ok'){
                                document.getElementById("loadnya").style.width = "0px";
                                swal({
                                    title: 'Sukses diproses',
                                    text: '',
                                    icon: 'success',
                                    buttons: {
                                        cancel: {
                                            text: 'Tutup',
                                            value: null,
                                            visible: true,
                                            className: 'btn btn-default',
                                            closeModal: true,
                                        },
                                        
                                    }
                                });
                                $('#modal-form').modal('hide');
                                $('#modal-form-all').modal('hide');
                                $('#tampil-form').html("");
                                var tables=$('#data-table-fixed-header').DataTable();
                                     tables.ajax.url("{{ url('transaksi-tetap/peminjaman-aset/getdatadetail')}}?peminjaman_id={{$id}}").load();
                            }else{
                                document.getElementById("loadnya").style.width = "0px";
                                
                                swal({
                                    title: 'Opps Error!',
                                    html:true,
                                    text:'ss',
                                    icon: 'error',
                                    buttons: {
                                        cancel: {
                                            text: 'Tutup',
                                            value: null,
                                            visible: true,
                                            className: 'btn btn-default',
                                            closeModal: true,
                                        },
                                        
                                    }
                                });
                                $('.swal-text').html('<div style="width:100%;background:#f2f2f5;padding:2%;text-align:left;font-size:13px">'+msg+'</div>')
                            }
                            
                            
                        }
                    });
            }
            function delete_data(id,act){
                if(act==2){
                    Swal.fire({
                        title: "Yakin menghapus user ini ?",
                        text: "data akan hilang dari data user  ini",
                        type: "warning",
                        icon: "error",
                        showCancelButton: true,
                        align:"center",
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes",
                        closeOnConfirm: false
                        }).then((result) => {
                            if (result.isConfirmed) {
                                $.ajax({
                                    type: 'GET',
                                    url: "{{url('master/obat/delete')}}",
                                    data: "id="+id+"&act="+act,
                                    success: function(msg){
                                        Swal.fire({
                                            title: "Sukses diproses",
                                            type: "warning",
                                            icon: "success",
                                            
                                            align:"center",
                                            
                                        });
                                        var tables=$('#data-table-fixed-header').DataTable();
                                            tables.ajax.url("{{ url('master/obat/getdata')}}").load();
                                    }
                                });
                                
                            }
                        
                    });
                }
                if(act==1){
                    Swal.fire({
                        title: "Yakin non aktifkan user ini ?",
                        text: "",
                        type: "warning",
                        icon: "info",
                        showCancelButton: true,
                        align:"center",
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes",
                        closeOnConfirm: false
                        }).then((result) => {
                            if (result.isConfirmed) {
                                $.ajax({
                                    type: 'GET',
                                    url: "{{url('master/obat/delete')}}",
                                    data: "id="+id+"&act="+act,
                                    success: function(msg){
                                        Swal.fire({
                                            title: "Sukses diproses",
                                            type: "warning",
                                            icon: "success",
                                            
                                            align:"center",
                                            
                                        });
                                        var tables=$('#data-table-fixed-header').DataTable();
                                            tables.ajax.url("{{ url('master/obat/getdata')}}").load();
                                    }
                                });
                                
                            }
                        
                    });
                }
                if(act==3){
                    Swal.fire({
                        title: "Yakin mengaktifkan user ini ?",
                        text: "",
                        type: "warning",
                        icon: "info",
                        showCancelButton: true,
                        align:"center",
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes",
                        closeOnConfirm: false
                        }).then((result) => {
                            if (result.isConfirmed) {
                                $.ajax({
                                    type: 'GET',
                                    url: "{{url('master/obat/delete')}}",
                                    data: "id="+id+"&act="+act,
                                    success: function(msg){
                                        Swal.fire({
                                            title: "Sukses diproses",
                                            type: "warning",
                                            icon: "success",
                                            
                                            align:"center",
                                            
                                        });
                                        var tables=$('#data-table-fixed-header').DataTable();
                                            tables.ajax.url("{{ url('master/obat/getdata')}}").load();
                                    }
                                });
                                
                            }
                        
                    });
                }
                
            } 
            
        </script>
@endpush