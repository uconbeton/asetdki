<input type="hidden" name="id" value="{{$id}}">
<input type="hidden" name="kategori_aset_id" value="{{$mst->id}}">

<div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">Kode Aset</label>
	</div>
	<div class="col-lg-5">
		<input type="text" class="form-control "  name="kode_aset" value="{{$data->kode_aset}}" placeholder="Enter.....">
	</div>
	
</div>
<div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">Nama Aset</label>
	</div>
	<div class="col-lg-8">
		<input type="text" class="form-control "  name="nama_aset" value="{{$data->nama_aset}}" placeholder="Enter.....">
	</div>
	
</div>
<div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">No Serial / Mesin / Pol</label>
	</div>
	<div class="col-lg-5">
		<input type="text" class="form-control "  name="serial_number" value="{{$data->serial_number}}" placeholder="Enter.....">
	</div>
	
</div>
<div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">Merk </label>
	</div>
	<div class="col-lg-9">
		<input type="text" class="form-control "  name="merk" value="{{$data->merk}}" placeholder="Enter.....">
	</div>
	
	
</div>
<div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">Tipe </label>
	</div>
	<div class="col-lg-9">
		<input type="text" class="form-control "  name="tipe" value="{{$data->tipe}}" placeholder="Enter.....">
	</div>
	
	
</div>
<div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">Asal Oleh / Sumber </label>
	</div>
	<div class="col-lg-7">
		<select class="default-select2 form-control " name="sumber_aset_id">
			<option value="">::. Pilih - -</option>
			@foreach(get_sumber_aset() as $gt)
				<option value="{{$gt->id}}" @if($data->sumber_aset_id==$gt->id) selected @endif >-  {{$gt->sumber_aset}}</option>
			@endforeach
		</select>
	</div>
	
	
</div>
<div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">Satuan & Jenis </label>
	</div>
	<div class="col-lg-3">
		<select class="default-select2 form-control " name="satuan_id">
			<option value="">::. Pilih - -</option>
			@foreach(get_satuan_aset() as $gt)
				<option value="{{$gt->id}}" @if($data->satuan_id==$gt->id) selected @endif >  {{$gt->satuan_aset}}</option>
			@endforeach
		</select>
	</div>
	<div class="col-lg-4">
		<select class="default-select2 form-control " name="jenis_aset_id">
			<option value="">::. Pilih - -</option>
			@foreach(get_jenis_aset() as $gt)
				<option value="{{$gt->id}}" @if($data->jenis_aset_id==$gt->id) selected @endif > {{$gt->jenis_aset}}</option>
			@endforeach
		</select>
	</div>
	
</div>

<div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">Spesifikasi</label>
	</div>
	<div class="col-lg-8">
		<textarea rows="3" class="form-control "  name="spesifikasi" " placeholder="Enter.....">{{$data->spesifikasi}}</textarea>
	</div>
	
</div>
<div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">Tanggal Masuk</label>
	</div>
	<div class="col-lg-3">
		<div class="input-group date" id="datetimepicker1">
			<input type="text" name="tanggal" value="{{$data->tanggal}}" class="form-control" />
			<div class="input-group-addon">
				<i class="fa fa-calendar"></i>
			</div>
		</div>
	</div>
	
</div>
<div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">Harga </label>
	</div>
	<div class="col-lg-3">
		<input type="text" class="form-control "  name="harga" value="{{$data->harga}}" style="text-align:right" placeholder="0">
	</div>
	
	
</div>
@if($id>0)
<div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">Kode QR</label>
	</div>
	<div class="col-lg-1" style="padding: 2px;background: #ebebed;text-align: center; line-height: 5;">
		<img src="data:image/png;base64,{!!DNS2D::getBarcodePNG(encoder($data->id), 'QRCODE',8,8)!!}" width="80%" alt="barcode"   />
	</div>
	<div class="col-lg-6" style="padding: 1%;background: yellow;text-align:center">
		<h4>QrCode Asset</h4>, Gunakan barcode ini untuk melihat deskripsi aset
	</div>
	
</div>
@endif
<!-- <div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">User Role</label>
	</div>
	<div class="col-lg-6">
		<select class="default-select2 form-control " name="role_id" >
			<option value="">Select --</option>
			<option value="1">Admin</option>
			<option value="2">User Monitoring</option>
			
		</select>
		
	</div>
	
</div>
<div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">Password</label>
	</div>
	<div class="col-lg-8">
		<input type="password" class="form-control "  name="password" value="{{$data->password}}" placeholder="Enter.....">
	</div>
	
</div> -->


<script>
	$(".default-select2").select2();
	$('#datetimepicker1').datepicker({
			format:'yyyy-mm-dd'
		});
	function hanyaAngka(evt) {
				
				var charCode = (evt.which) ? evt.which : event.keyCode
				if ((charCode > 47 && charCode < 58 ) || (charCode > 96 && charCode < 123 ) || charCode==95 ){
					
					return true;
				}else{
					return false;
				}
		
				// 	return false;
				// return true;
				// alert(charCode)
			}
</script>
