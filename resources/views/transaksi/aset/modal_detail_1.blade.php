<input type="hidden" name="id" value="{{$id}}">
<input type="hidden" name="kategori_aset_id" value="{{$mst->id}}">
<style>
	.ttd{
		padding:4px;
		padding-right:10px;
		font-size:14px;
		vertical-align:top;
	}
	.ttdb {
		padding: 4px;
		font-size: 14px;
		font-family: unset;
		border-bottom: solid 1px #ffd5d5;
	}
</style>
<table width="100%">
	<tr>
		<td class="ttd"  width="20%" style="text-align:right"><b>Kode Aset</b></td>
		<td class="ttd" style="text-align:right" width="5%">:</td>
		<td class="ttdb" >{{$data->kode_aset}}</td>
	</tr>
	<tr>
		<td class="ttd"  style="text-align:right"><b>Nama Aset</b></td>
		<td class="ttd" style="text-align:right">:</td>
		<td class="ttdb" >{{$data->nama_aset}}</td>
	</tr>
	<tr>
		<td class="ttd"  style="text-align:right"><b>No Registrasi</b></td>
		<td class="ttd" style="text-align:right">:</td>
		<td class="ttdb" >{{$data->no_register}}</td>
	</tr>
	<tr>
		<td class="ttd"  style="text-align:right"><b>Merk</b></td>
		<td class="ttd" style="text-align:right">:</td>
		<td class="ttdb" >{{$data->merk}}</td>
	</tr>
	<tr>
		<td class="ttd"  style="text-align:right"><b>Tipe</b></td>
		<td class="ttd" style="text-align:right">:</td>
		<td class="ttdb" >{{$data->tipe}}</td>
	</tr>
	<tr>
		<td class="ttd"  style="text-align:right"><b>Satuan</b></td>
		<td class="ttd" style="text-align:right">:</td>
		<td class="ttdb" >{{$data->satuan}}</td>
	</tr>
	<tr>
		<td class="ttd"  style="text-align:right"><b>Tanggal</b></td>
		<td class="ttd" style="text-align:right">:</td>
		<td class="ttdb" >{{$data->tanggal}}</td>
	</tr>
			<?php
				$cekaset=url('/aset-pusdatin/'.encoder($data->id));
			?>
	<tr>
		<td class="ttd"  style="text-align:right"><b>Pengguna</b></td>
		<td class="ttd" style="text-align:right">:</td>
		<td class="ttdb" >@if($data->nik!="" || $data->nik!=null) [{{$data->nik}}] {{$data->nama_pengguna}} @else <i>Tidak Tersedia</i> @endif</td>
	</tr>
	<tr>
		<td class="ttd"  style="text-align:right"><b>Harga (Rp)</b></td>
		<td class="ttd" style="text-align:right">:</td>
		<td class="ttdb" >Rp. {{uang($data->harga)}}</td>
	</tr>
	<tr>
		<td class="ttd"  style="text-align:right"><b>QrCode</b></td>
		<td class="ttd" style="text-align:right">:</td>
		<td class="ttdb" >
			
			<img src="data:image/png;base64,{!!DNS2D::getBarcodePNG($cekaset, 'QRCODE',8,8)!!}" width="20%" alt="barcode"   />
		</td>
	</tr>
	
</table>


<!-- <div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">User Role</label>
	</div>
	<div class="col-lg-6">
		<select class="default-select2 form-control " name="role_id" >
			<option value="">Select --</option>
			<option value="1">Admin</option>
			<option value="2">User Monitoring</option>
			
		</select>
		
	</div>
	
</div>
<div class="row mb-1">
	<div class="col-lg-3 label-col">
		<label for="nameInput" class="form-label">Password</label>
	</div>
	<div class="col-lg-8">
		<input type="password" class="form-control "  name="password" value="{{$data->password}}" placeholder="Enter.....">
	</div>
	
</div> -->


<script>
	$(".default-select2").select2();
	$('#datetimepicker1').datepicker({
			format:'yyyy-mm-dd'
		});
	function hanyaAngka(evt) {
				
				var charCode = (evt.which) ? evt.which : event.keyCode
				if ((charCode > 47 && charCode < 58 ) || (charCode > 96 && charCode < 123 ) || charCode==95 ){
					
					return true;
				}else{
					return false;
				}
		
				// 	return false;
				// return true;
				// alert(charCode)
			}
</script>
