
@extends('layouts.app')
@push('datatable')
<script src="{{url_plug()}}/assets/plugins/chart.js/dist/Chart.min.js"></script>
    <script type="text/javascript">
        /*
        Template Name: Color Admin - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
        Version: 4.6.0
        Author: Sean Ngu
        Website: http://www.seantheme.com/color-admin/admin/
        */

        $.getJSON("{{url('home/getdatadashboardpengguna')}}").done( function (results){
			var labels=[];
			var data=[];
			var labels=results.map(function (item){
				return item.bulan;
			});
			var data=results.map(function (item){
				return item.peminjaman;
			});
			var data2=results.map(function (item){
				return item.persediaan;
			});
			createChart(labels,data,data2);
			
		});
        function createChart(labels,data,data2){
			

			var barChartData = {
				labels: labels,
				datasets: [{
					label: 'Peminjaman',
					borderWidth: 2,
					borderColor: COLOR_INDIGO,
					backgroundColor: 'aqua',
					data: data
				},{
					label: 'Persediaan',
					borderWidth: 2,
					borderColor: COLOR_INDIGO,
					backgroundColor: 'orange',
					data: data2
				}]
			};
			
			var ctx2 = document.getElementById('bar-chart').getContext('2d');
			var barChart = new Chart(ctx2, {
				type: 'bar',
				data: barChartData
			});

		}
        function load_data(){
			$.ajax({ 
                type: 'GET', 
                url: "{{ url('home/getdatadashboard')}}", 
                data: { id: 1 }, 
                dataType: 'json',
                beforeSend: function() {
                    $('#tampil-urutan').html("")
                    
                },
                success: function (data) {
                    
                    $.each(data, function(i, result){
                        if(result.total>0){
                            var color="red";
                        }else{
                            var color="blueblue";
                        }
                        var tampil='<div class="col-xl-3 col-md-6">'
                                        +'<div class="widget widget-stats bg-'+result.color+'">'
                                            +'<div class="stats-icon"><i class="fa fa-desktop"></i></div>'
                                            +'<div class="stats-info">'
                                                +'<h4 style="text-transform:uppercase">'+result.kategori+'</h4>'
                                                +'<p>'+result.total+'</p>	'
                                            +'</div>'
                                            +'<div class="stats-link">'
                                                +'<a href="javascript:;">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>'
                                            +'</div>'
                                        +'</div>'
                                    +'</div>';
                        $("#tampil-urutan").append(tampil);
                    }); 
                    $.each(data, function(i, result){
                        if(result.total>0){
                            var color="red";
                        }else{
                            var color="blueblue";
                        }
                        var tampil='<tr>'
										+'<td><label class="label label-'+result.color+'">'+result.kategori+'</label></td>'
										+'<td>'+result.total+'</td>'
									+'</tr>';
                        $("#tampil-tabel").append(tampil);
                    }); 
                    
                }
			});
		}
        // function show_data() {
        //     if ($('#data-table-fixed-header').length !== 0) {
        //         var table=$('#data-table-fixed-header').DataTable({
        //             lengthMenu: [20, 40, 60],
        //             lengthChange:false,
        //             fixedHeader: {
        //                 header: true,
        //                 headerOffset: $('#header').height()
        //             },
        //             responsive: true,
        //             ajax:"{{ url('user/getdata')}}",
        //             dom: 'lrtip',
		// 			columns: [
        //                 { data: 'id', render: function (data, type, row, meta) 
		// 					{
		// 						return meta.row + meta.settings._iDisplayStart + 1;
		// 					} 
		// 				},
		// 				{ data: 'username' },
						
		// 			],
		// 			language: {
		// 				paginate: {
		// 					// remove previous & next text from pagination
		// 					previous: '<< previous',
		// 					next: 'Next>>'
		// 				}
		// 			}
        //         });
        //     }
        // };


        $(document).ready(function() {
			load_data();
		});

		
    </script>
@endpush
@section('content')
    
        <div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb float-xl-right">
				<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
				<li class="breadcrumb-item active">Dashboard</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Dashboard <small></small></h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row" id="tampil-urutan">
				
				
			</div>

            <div class="row">
				<!-- begin col-8 -->
				<div class="col-xl-8">
					<!-- begin panel -->
					<div class="panel panel-inverse" data-sortable-id="index-1">
						<div class="panel-heading">
                            <h4 class="panel-title" style="color:#000">Penggunaan Aset</h4>
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						<div class="panel-body pr-1">
                            <canvas id="bar-chart" data-render="chart-js"></canvas>
						</div>
					</div>
					<div class="panel panel-inverse" data-sortable-id="index-1">
						<div class="panel-heading">
                            <h4 class="panel-title" style="color:#000">Penggunaan Aset</h4>
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						<div class="panel-body pr-1">
                            <div id="apex-mixed-chart"></div>
						</div>
					</div>
				</div>
				<div class="col-xl-4">
                    <div class="panel panel-inverse" data-sortable-id="index-6">
						<div class="panel-heading">
							<h4 class="panel-title" style="color:#000">Analytics Kategori Aset</h4>
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						<div class="table-responsive">
							<table class="table table-valign-middle table-panel mb-0">
								<thead>
									<tr>	
										<th>Kategori</th>
										<th>Total</th>
									</tr>
								</thead>
								<tbody id="tampil-tabel">
									
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- end row -->
			<!-- begin row -->
			
		</div>     
@endsection
@push('ajax')
        
        <script type="text/javascript">
			
			function tambah(id){
				$('#modal-form').modal('show')
				$('#tampil-form').load("{{url('user/modal')}}?id="+id)
			} 
            function hanyaAngka(evt) {
				
				var charCode = (evt.which) ? evt.which : event.keyCode
				if ((charCode > 47 && charCode < 58 ) || (charCode > 96 && charCode < 123 ) || charCode==95 ){
					
					return true;
				}else{
					return false;
				}
		    }
            function simpan_data(){
            
                var form=document.getElementById('mydataform');
                    $.ajax({
                        type: 'POST',
                        url: "{{ url('user') }}",
                        data: new FormData(form),
                        contentType: false,
                        cache: false,
                        processData:false,
                        beforeSend: function() {
                            document.getElementById("loadnya").style.width = "100%";
                        },
                        success: function(msg){
                            var bat=msg.split('@');
                            if(bat[1]=='ok'){
                                document.getElementById("loadnya").style.width = "0px";
                                Swal.fire({
                                    title:"Notifikasi",
                                    html:'Create User Succes ',
                                    icon:"success",
                                    confirmButtonText: 'Close',
                                    confirmButtonClass:"btn btn-info w-xs mt-2",
                                    buttonsStyling:!1,
                                    showCloseButton:!0
                                });
                                $('#modal-form').modal('hide')
				                $('#tampil-form').html("")
                                var tables=$('#data-table-fixed-header').DataTable();
                                        tables.ajax.url("{{ url('user/getdata')}}").load();
                            }else{
                                document.getElementById("loadnya").style.width = "0px";
                                Swal.fire({
                                    title:"Notifikasi",
                                    html:'<div style="background:#f2f2f5;padding:1%;text-align:left;font-size:13px">'+msg+'</div>',
                                    icon:"error",
                                    confirmButtonText: 'Close',
                                    confirmButtonClass:"btn btn-danger w-xs mt-2",
                                    buttonsStyling:!1,
                                    showCloseButton:!0
                                });
                            }
                            
                            
                        }
                    });
            }
            function delete_data(id,act){
                if(act==2){
                    Swal.fire({
                        title: "Yakin menghapus user ini ?",
                        text: "data akan hilang dari data user  ini",
                        type: "warning",
                        icon: "error",
                        showCancelButton: true,
                        align:"center",
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes",
                        closeOnConfirm: false
                        }).then((result) => {
                            if (result.isConfirmed) {
                                $.ajax({
                                    type: 'GET',
                                    url: "{{url('user/delete')}}",
                                    data: "id="+id+"&act="+act,
                                    success: function(msg){
                                        Swal.fire({
                                            title: "Sukses diproses",
                                            type: "warning",
                                            icon: "success",
                                            
                                            align:"center",
                                            
                                        });
                                        var tables=$('#data-table-fixed-header').DataTable();
                                            tables.ajax.url("{{ url('user/getdata')}}").load();
                                    }
                                });
                                
                            }
                        
                    });
                }
                if(act==1){
                    Swal.fire({
                        title: "Yakin non aktifkan user ini ?",
                        text: "",
                        type: "warning",
                        icon: "info",
                        showCancelButton: true,
                        align:"center",
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes",
                        closeOnConfirm: false
                        }).then((result) => {
                            if (result.isConfirmed) {
                                $.ajax({
                                    type: 'GET',
                                    url: "{{url('user/delete')}}",
                                    data: "id="+id+"&act="+act,
                                    success: function(msg){
                                        Swal.fire({
                                            title: "Sukses diproses",
                                            type: "warning",
                                            icon: "success",
                                            
                                            align:"center",
                                            
                                        });
                                        var tables=$('#data-table-fixed-header').DataTable();
                                            tables.ajax.url("{{ url('user/getdata')}}").load();
                                    }
                                });
                                
                            }
                        
                    });
                }
                if(act==3){
                    Swal.fire({
                        title: "Yakin mengaktifkan user ini ?",
                        text: "",
                        type: "warning",
                        icon: "info",
                        showCancelButton: true,
                        align:"center",
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes",
                        closeOnConfirm: false
                        }).then((result) => {
                            if (result.isConfirmed) {
                                $.ajax({
                                    type: 'GET',
                                    url: "{{url('user/delete')}}",
                                    data: "id="+id+"&act="+act,
                                    success: function(msg){
                                        Swal.fire({
                                            title: "Sukses diproses",
                                            type: "warning",
                                            icon: "success",
                                            
                                            align:"center",
                                            
                                        });
                                        var tables=$('#data-table-fixed-header').DataTable();
                                            tables.ajax.url("{{ url('user/getdata')}}").load();
                                    }
                                });
                                
                            }
                        
                    });
                }
                
            } 
            function kembali_diproses(id,act){
                if(act==2){
                    Swal.fire({
                        title: "Yakin menghapus user ini ?",
                        text: "data akan hilang dari data user  ini",
                        type: "warning",
                        icon: "error",
                        showCancelButton: true,
                        align:"center",
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes",
                        closeOnConfirm: false
                        }).then((result) => {
                            if (result.isConfirmed) {
                                $.ajax({
                                    type: 'GET',
                                    url: "{{url('user/delete')}}",
                                    data: "id="+id+"&act="+act,
                                    success: function(msg){
                                        Swal.fire({
                                            title: "Sukses diproses",
                                            type: "warning",
                                            icon: "success",
                                            
                                            align:"center",
                                            
                                        });
                                        var tables=$('#data-table-fixed-header').DataTable();
                                            tables.ajax.url("{{ url('user/getdata')}}").load();
                                    }
                                });
                                
                            }
                        
                    });
                }
                if(act==1){
                    Swal.fire({
                        title: "Yakin non aktifkan user ini ?",
                        text: "",
                        type: "warning",
                        icon: "info",
                        showCancelButton: true,
                        align:"center",
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes",
                        closeOnConfirm: false
                        }).then((result) => {
                            if (result.isConfirmed) {
                                $.ajax({
                                    type: 'GET',
                                    url: "{{url('user/delete')}}",
                                    data: "id="+id+"&act="+act,
                                    success: function(msg){
                                        Swal.fire({
                                            title: "Sukses diproses",
                                            type: "warning",
                                            icon: "success",
                                            
                                            align:"center",
                                            
                                        });
                                        var tables=$('#data-table-fixed-header').DataTable();
                                            tables.ajax.url("{{ url('user/getdata')}}").load();
                                    }
                                });
                                
                            }
                        
                    });
                }
                if(act==3){
                    Swal.fire({
                        title: "Yakin mengaktifkan user ini ?",
                        text: "",
                        type: "warning",
                        icon: "info",
                        showCancelButton: true,
                        align:"center",
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes",
                        closeOnConfirm: false
                        }).then((result) => {
                            if (result.isConfirmed) {
                                $.ajax({
                                    type: 'GET',
                                    url: "{{url('user/delete')}}",
                                    data: "id="+id+"&act="+act,
                                    success: function(msg){
                                        Swal.fire({
                                            title: "Sukses diproses",
                                            type: "warning",
                                            icon: "success",
                                            
                                            align:"center",
                                            
                                        });
                                        var tables=$('#data-table-fixed-header').DataTable();
                                            tables.ajax.url("{{ url('user/getdata')}}").load();
                                    }
                                });
                                
                            }
                        
                    });
                }
                
            } 
        </script>
@endpush