
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Color Admin | Coming Soon Page</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link href="{{url_plug()}}/assets/css/default/app.min.css" rel="stylesheet" />
	<!-- ================== END BASE CSS STYLE ================== -->
	
	<!-- ================== BEGIN PAGE CSS STYLE ================== -->
	<link href="{{url_plug()}}/assets/plugins/countdown/jquery.countdown.css" rel="stylesheet" />
	<!-- ================== END PAGE CSS STYLE ================== -->
</head>
<body class="bg-white pace-top">
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show">
		<span class="spinner"></span>
	</div>
	<!-- end #page-loader -->
	
	<!-- begin #page-container -->
	<div id="page-container" class="page-container fade">
	  <!-- begin coming-soon -->
		<div class="coming-soon">
			<!-- begin coming-soon-header -->
			<div class="coming-soon-header">
				<div class="bg-cover"></div>
				<div class="brand">
						<img src="{{url_plug()}}/img/jakarta.png" width="10%">
						<h5>ASET MANAGEMENT</h5><h6>Dinas Cipta Karya, Tata Ruang dan Pertanaha</h6>
						
				</div>
				<div class="desc">
				<style>
					.ttd{
						padding:4px;
						padding-right:10px;
						font-size:14px;
						vertical-align:top;
					}
					.ttdb {
						padding: 4px;
						font-size: 14px;
						font-family: unset;
						border-bottom: solid 1px #ffd5d5;
					}
				</style>
				<div class="alert alert-success fade show">
				<table width="60%" align="center">
					<tr>
						<td class="ttd" style="text-align:center"><b>Kode Aset</b></td>
					</tr>
					<tr>
						<td class="ttdb" >{{$data->kode_aset}}</td>
					</tr>
					
					<tr>
						<td class="ttd"><b>No Registrasi</b></td>
					</tr>
					<tr>
						<td class="ttdb" >{{$data->no_register}}</td>
					</tr>

					<tr>
						<td class="ttd"><b>Merk</b></td>
					</tr>
					<tr>
						<td class="ttdb" >{{$data->merk}}</td>
					</tr>

					<tr>
						<td class="ttd"><b>Satuan</b></td>
					</tr>
					<tr>
						<td class="ttdb" >{{$data->satuan}}</td>
					</tr>

					<tr>
						<td class="ttd"><b>QrCode</b></td>
					</tr>
					<tr>
						<td class="ttdb" >
						<?php
								$cekaset=url('/aset-pusdatin/'.encoder($data->id));
							?>
							<img src="data:image/png;base64,{!!DNS2D::getBarcodePNG($cekaset, 'QRCODE',8,8)!!}" width="40%" alt="barcode"   />
						</td>
					</tr>
					
					
				</table>
				</div>
				</div>
				
			</div>
			
		</div>
		
	</div>
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="{{url_plug()}}/assets/js/app.min.js"></script>
	<script src="{{url_plug()}}/assets/js/theme/default.min.js"></script>
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="{{url_plug()}}/assets/plugins/countdown/jquery.plugin.min.js"></script>
	<script src="{{url_plug()}}/assets/plugins/countdown/jquery.countdown.min.js"></script>
	<script src="{{url_plug()}}/assets/js/demo/coming-soon.demo.js"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->
</body>
</html>